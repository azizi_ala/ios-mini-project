
import UIKit
import Photos
import Alamofire
import AlamofireImage

class AddNewPostServiceViewController: UIViewController ,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate , UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var typeofgender: UISegmentedControl!
    @IBOutlet weak var typeofservice: UISegmentedControl!
    @IBOutlet weak var typeofrace: UISegmentedControl!
    @IBOutlet weak var agepet: UITextField!
    @IBOutlet weak var petimg: UIImageView!
    @IBOutlet weak var nameofpet: UITextField!
    @IBOutlet weak var desc: UITextView!
    var status = "Normal"
    var  petController = PetController()
    var genderData : [String] = [String]()
    var pickedImageProduct = UIImage()
    let imagePicker = UIImagePickerController()
    var gender = ""
    var race = ""
    let UserDefault = UserDefaults.standard
    var pet:Pet = Pet()
    override func viewDidLoad() {
        super.viewDidLoad()
        genderData = ["Female", "Male"]
         imagePicker.delegate = self
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let valueSelected = genderData[row] as String
        pet.pet_sex = valueSelected
        print(valueSelected)
        gender = valueSelected
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController,
                 didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Error: \(info)")
        }
        let imageData:Data = (selectedImage ).jpegData(compressionQuality: 1.0)!
        pickedImageProduct = selectedImage
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as! URL
        
        let filename = fileUrl.lastPathComponent
        pet.pet_picture = filename
        print(fileUrl.lastPathComponent)
        UploadImage.upload(imageData: imageData, name: filename)
        self.petimg.image = pickedImageProduct
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func addImage(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    @IBAction func btBack(_ sender: Any) {
          dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func typeofRace(_ sender: Any) {
        let seg = typeofrace.selectedSegmentIndex
        if seg == 0 {
            pet.pet_race = "Mountain"
            race = "Mountain"
        }else if seg == 1 {
            pet.pet_race = "Oasis"
            race = "Oasis"}
        else{
            pet.pet_race = "Beach"
            race = "Beach"
        }}
    
    @IBAction func typeofGender(_ sender: Any) {
        let seg = typeofgender.selectedSegmentIndex
               if seg == 0 {
                pet.pet_sex = "Unique"
                   gender = "Unique"
               }else{
                pet.pet_sex = "Mixed"
                   gender = "Mixed"
               }
               
    }
    @IBAction func typeofService(_ sender: Any) {
        let seg = typeofservice.selectedSegmentIndex
        if seg == 0 {
            status = "Lost"
            pet.pet_status = "Lost"
        }else if seg == 1 {
            pet.pet_status = "Adoption"
            status = "Adoption"
        }else{
            pet.pet_status = "Normal"
            status = "Normal"
        }
    }

    @IBAction func dismisskeyboard(_ sender: Any) {
        //  typeofservice.resignFirstResponder()
        desc.resignFirstResponder()
        //  racepet.resignFirstResponder()
        //poidpet.resignFirstResponder()
        agepet.resignFirstResponder()
        nameofpet.resignFirstResponder()
    }
    @IBAction func NextStep(_ sender: Any) {
        if (desc.text! == "") || (nameofpet.text! == "") || (agepet.text!  == "") || (pet.pet_race == "" ) || (pet.pet_sex == "") || (pet.pet_picture == "") ||
            (pet.pet_status == "" ){
            let alert = UIAlertController(title: "", message: "Please Fill All fields", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert,animated: true,completion: nil)
            
        }else{
            pet.pet_name = nameofpet.text!
            pet.pet_desc = desc.text!
            pet.pet_age = agepet.text!
            pet.pet_race = race
            pet.pet_status = status
            pet.owner = UserDefault.integer(forKey: "user_id")
            petController.AddPet(animal: pet , completionHandler:{response in }
            )
    
        }
        
        let next = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        self.present(next, animated: true, completion: nil)
        
    }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   
    }
}
