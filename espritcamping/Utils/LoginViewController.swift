

import UIKit
import UserNotifications
import Firebase
class LoginViewController: UIViewController {
    let UserDefault = UserDefaults.standard
    @IBOutlet var EmailField: UITextField!
    @IBOutlet var Password: UITextField!
    var validation = Validation()
    var alertView:UIView = UIView()
    var emailError = "Email not valid, must be like this X@X.X"
    var passwordError = "Password not valid, must be at least 8 characters and contains 1 letter and 1 number "
    var UserControler = UserController()
    var PetControler = PetController()
    var result = User()
    
    @IBOutlet weak var container: UILabel!
    override func viewDidLoad() {
        
        container.layer.borderWidth = 4
        container.layer.cornerRadius = container.frame.height/4
        container.layer.borderColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        container.layer.backgroundColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        
        
        /*let content = UNMutableNotificationContent()
        content.title = "Title"
        content.body = "body"
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)*/
        super.viewDidLoad()
    }
    
    @IBAction func OnClick(_ sender: UIButton) {
        
        var error = String()
        guard let mail = EmailField.text, let pasword = Password.text else {
            return
        }
        let isValidatePass = self.validation.validatePassword(password: pasword)
        let isValidateEmail = self.validation.validateEmailId(emailID: mail)
        if(isValidatePass && isValidateEmail )
        {
            UserControler.Login(email: mail, password: pasword, completionHandler: {
                response in
                self.UserDefault.set(response.user_id, forKey: "user_id")
                self.UserDefault.set(response.user_username, forKey: "user_username")
                self.UserDefault.set(response.user_email, forKey: "user_email")
                self.UserDefault.set(response.user_phonenumber, forKey: "user_phonenumber")
                self.UserDefault.set(response.user_address , forKey: "user_address")
                self.UserDefault.set(response.user_lastname,forKey:"user_lastname")
                self.UserDefault.set(response.user_firstname,forKey: "user_firstname")
                self.UserDefault.set(0,forKey:"signup")
                self.UserDefault.set(response.user_picture!,forKey: "user_picture")
                 self.UserDefault.synchronize()
                 if  self.UserDefault.string(forKey: "user_id") != nil{
                  /*  let pushManager = PushNotificationManager(userID: self.UserDefault.string(forKey: "user_id")!)
                                pushManager.registerForPushNotifications()
                    
                                FirebaseApp.configure()
                                pushManager.updateFirestorePushTokenIfNeeded()*/
                                
                let next = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
                    self.present(next, animated: true, completion: nil)
                 }
            })
        }
        if (isValidateEmail == false) {
            
            EmailField.rightViewMode = .always
            EmailField.rightView = alertViewOnRightSideOfTextField()
            error += emailError + "\n"
        }else
        {
            EmailField.rightViewMode = .never
        }
        
        if (isValidatePass == false) {
            
            Password.rightViewMode = .always
            Password.rightView = alertViewOnRightSideOfTextField()
            error += passwordError + "\n"
            
        }else{
            Password.rightViewMode = .never
        }
        self.view.makeToast(error, duration: 3.0, position: .bottom)
        
    }
    func alertViewOnRightSideOfTextField() -> UIView {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 27, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage.init(named: "errorImage")
        alertView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        alertView.addSubview(imageView)
        return alertView
    }
   

}

