

import UIKit
import Firebase

class SignupViewController: UIViewController {
    @IBOutlet var EmailField: UITextField!
    @IBOutlet var Password: UITextField!
    @IBOutlet var Username: UITextField!
    @IBOutlet var Phonenumber: UITextField!
    @IBOutlet weak var container: UILabel!
    var pin:UITextField! = UITextField()
    var usernameError = "Username not valid"
    var emailError = "Email not valid, must be like this X@X.X"
    var passwordError = "Password not valid, must be at least 8 characters and contains 1 letter and 1 number "
    var phoneError = "Phone number not valid, only numeric"
    var UserControler = UserController()
    var result = User()
    var validation = Validation()
    var addAlert: UIAlertController!
    var alertView:UIView = UIView()
    var prefix:String! = ""
    let UserDefault = UserDefaults.standard
    @IBOutlet weak var CountryPhone: UITextField!
    @IBOutlet weak var country: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.borderWidth = 4
        container.layer.cornerRadius = container.frame.height/4
        container.layer.borderColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        container.layer.backgroundColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        Phonenumber.layer.borderColor = UIColor.black.cgColor
        Phonenumber.layer.borderWidth = 1
        Username.layer.borderColor = UIColor.black.cgColor
        Username.layer.borderWidth = 1
        EmailField.layer.borderColor = UIColor.black.cgColor
        EmailField.layer.borderWidth = 1
        Password.layer.borderWidth = 1
        Password.layer.borderColor = UIColor.black.cgColor
        CountryPhone.layer.borderColor = UIColor.black.cgColor
        CountryPhone.layer.borderWidth = 1
        let tapped = MyTapGesture.init(target: self, action: #selector(SignupViewController.ChooseCountry))
        CountryPhone.addGestureRecognizer(tapped)
        CountryPhone.isUserInteractionEnabled = true
        country.addGestureRecognizer(tapped)
        country.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func SignUp(_ sender: UIButton) {
        var error = String()
        guard let name = Username.text, let email = EmailField.text, let password = Password.text,
            let phone = Phonenumber.text else {
                return
        }
        
        let isValidateName = self.validation.validateName(name: name)
        let isValidateEmail = self.validation.validateEmailId(emailID: email)
        let isValidatePass = self.validation.validatePassword(password: password)
        let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phone)
        if ( isValidateEmail && isValidatePass && isValidateName && isValidatePhone )
        {
            self.sendSms()
            self.addAlert = UIAlertController(title: "Let's share", message: "please fill the fields", preferredStyle: .alert)
            self.addAlert.addTextField(configurationHandler: {
                (textfield : UITextField!) in
                textfield.placeholder = "Type the verification code"
                self.pin = textfield
            })
            addAlert.addAction(UIAlertAction(title:"Verify",style: .default,handler : {
                (response:UIAlertAction!) in
                self.getVerificationCode()
            }))
            
            addAlert.addAction(UIAlertAction(title: "Re-send code", style: .default, handler: {
                response in
                self.sendSms()
            }))
            addAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(self.addAlert, animated: true, completion: nil)
            /*UserControler.SignUp(utilisateur: User(email: email,username: name,pwd: password,phonenumber: phone) , completionHandler: {
                response in
               self.StockUser(response: response)
                let next = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
                self.present(next, animated: true, completion: nil)
                           
                       })*/
        }
        if (isValidateName == false) {
            
            Username.rightViewMode = .always
            Username.rightView = alertViewOnRightSideOfTextField()
            error += usernameError + "\n"
        }
        
        if (isValidateEmail == false) {
            
            EmailField.rightViewMode = .always
            EmailField.rightView = alertViewOnRightSideOfTextField()
            error += email + "\n"
        }
        
        if (isValidatePass == false) {
            
            Password.rightViewMode = .always
            Password.rightView = alertViewOnRightSideOfTextField()
            error += passwordError + "\n"
        }
        
        if (isValidatePhone == false) {
        
            Phonenumber.rightViewMode = .always
            Phonenumber.rightView = alertViewOnRightSideOfTextField()
            error += phoneError     }

        self.view.makeToast(error, duration: 3.0, position: .bottom)    
    }
    
    func alertViewOnRightSideOfTextField() -> UIView {
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 27, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage.init(named: "errorImage")
        
        alertView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        alertView.addSubview(imageView)
        
        return alertView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ( segue.destination.self === TabBarController.self)
        {let svc = segue.destination as! TabBarController
            svc.user = self.result
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func toLogin(_ sender: Any) {
        self.performSegue(withIdentifier: "toLogin", sender: nil)
        }
    
    func StockUser(response:User)
    {
        self.UserDefault.set(response.user_address,forKey: "user_address")
        self.UserDefault.set(response.user_email,forKey: "user_email")
        self.UserDefault.set(response.user_id,forKey: "user_id")
        self.UserDefault.set(response.user_picture,forKey: "user_picture")
        self.UserDefault.set(response.user_lastname,forKey:"user_lastname")
        self.UserDefault.set(response.user_username,forKey:"user_username" )
        self.UserDefault.set(response.user_firstname,forKey: "user_firstname")
        self.UserDefault.set(response.password,forKey: "password")
        self.UserDefault.set(response.user_phonenumber,forKey: "user_phonenumber")
        self.UserDefault.set(1,forKey:"signup")
    }
    
    // MARK: - SMS Verification
    var verifyId:String = ""
    func sendSms() {
       
        print("\(CountryPhone.text!)\(Phonenumber.text!)")
        SmshostingVerify.sendPinWithPhoneNumberAndText(phoneNumber: "\(self.CountryPhone.text!.substring(from: self.CountryPhone.text!.index(after: self.CountryPhone.text!.startIndex)))\(Phonenumber.text!)", text:"Hey \(Username.text!) , you need to enter this verification ${verify_code} to validate your account", sandbox:false, completion: {
            (result: [String:Any]) in
            DispatchQueue.main.async {
                if(result["errorCode"] == nil){
                    //Request Done
                    //Pin sent, do what you want...

                    //save verifyId
                    self.verifyId = result["verify_id"] as! String
                    print(self.verifyId)
                }
                else{
                    
                    self.view.makeToast("There's a problem with our server, please try later", duration: 3.0, position: .bottom)
                   
                }
            }
        })
    }
    
    
    
    func getVerificationCode()
    {
        guard let name = Username.text, let email = EmailField.text, let password = Password.text,
                   let phone = Phonenumber.text else {
                       return
               }
        print(pin.text!)
        SmshostingVerify.verifyWithIdAndCode(verifyId: verifyId, verifyCode: pin.text!, completion: {
            (result: [String:Any]) in
            DispatchQueue.main.async {
                if(result["errorCode"] == nil){
                    //Request Done
                    if(result["verify_status"] != nil){
                        let statusString:String = result["verify_status"] as! String
                        if(statusString == "VERIFIED"){
                            //Verification done!
                            self.UserControler.SignUp(utilisateur: User(email: email,username: name,pwd: password,phonenumber: phone) , completionHandler: {
                                           response in
                                          self.StockUser(response: response)
                                           let next = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
                                      /*      let pushManager = PushNotificationManager(userID: self.UserDefault.string(forKey: "user_id")!)
                                            pushManager.registerForPushNotifications()
                                
                                            FirebaseApp.configure()
                                            pushManager.updateFirestorePushTokenIfNeeded()*/
                                            
                                           self.present(next, animated: true, completion: nil)
                                                      
                                                  })
                        }
                        else{
                                  self.view.makeToast("The pin code is invalid , please try again", duration: 3.0, position: .bottom)
                                    
                        }
                    }
                }
                else{
                    print(result["errorCode"])
                    //Request Error
                    //Verification failed, handle error...
                }
            }
        })
    }
    
    @objc func ChooseCountry(recognizer : MyTapGesture)
    {
        let alert = UIAlertController(style: .actionSheet, title: "Phone Codes")
        alert.addLocalePicker(type: .phoneCode) { info in
            self.CountryPhone.text = info?.phoneCode
            print("\(self.CountryPhone.text!.substring(from: self.CountryPhone.text!.index(after: self.CountryPhone.text!.startIndex)))")
        }
        self.present(alert,animated: true,completion: nil)
        
        /*alert.addLocalePicker(type: .phoneCode){
            info in
            print(info)
        }
        alert.show()*/
    }
    
}
