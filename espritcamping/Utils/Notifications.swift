
import Foundation
import UIKit
import SwiftEntryKit

func WelcomeNotif(title:String?,description:String?,picture:String?)
{
    var attributes = EKAttributes.topFloat
    attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(.cyan), EKColor(.cyan)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
    attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
    attributes.statusBar = .dark
    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
    let title = EKProperty.LabelContent(
        text: title as! String,
        style: EKProperty.LabelStyle(
            font: MainFont.bold.with(size: 16),
            color: .black)
    )
    
    let description = EKProperty.LabelContent(
        text: description as! String,
        style: EKProperty.LabelStyle(
            font: MainFont.light.with(size: 14),
            color: .black
        )
    )
    let image = EKProperty.ImageContent(image: UIImage(named: picture!)!, size: CGSize(width: 35, height: 35))
    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
    let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)

  let contentView = EKNotificationMessageView(with: notificationMessage)
    
    SwiftEntryKit.display(entry: contentView, using: attributes)
}
