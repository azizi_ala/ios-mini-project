

import Foundation

class Dateconverter {
    
    var format:String
    init(format:String){self.format=format}
    static func StringToDate(date:String?) -> Date?
    {
       
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss"
        let c :String = date!
        let d : Date? = formatter.date(from: c)
        return d
    }
    
    static func DateToString(date:Date?) -> String?
    {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss"
        let d = date as! Date
        let t = formatter.string(from: d)
        return t
        
    }
}
