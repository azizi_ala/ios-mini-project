
import UIKit
import Alamofire
import AlamofireImage

class PetAdoptViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    var idpet:Int = 0
       var UserControler = UserController()
       var user = User()
       var iduser:Int = 0
    let URL_ALL_PetForAdoption = "http://127.0.0.1:3000/petrescue/pets/status/Adoption"
    let URL_UserById = "http://127.0.0.1:3000/petrescue/"
    @IBOutlet weak var tableView: UITableView!
    
        var adoptPetss : NSArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request(URL_ALL_PetForAdoption).responseJSON
        {
                response in
                self.adoptPetss = response.result.value as! NSArray
                self.tableView.reloadData()
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
    dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return adoptPetss.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adoptPets")
        
        let contentView = cell?.viewWithTag(0)
        
        let profilImage = contentView?.viewWithTag(1) as! UIImageView
        let pubImage = contentView?.viewWithTag(2) as! UIImageView
        let usernameText = contentView?.viewWithTag(3) as! UILabel
        let breedText = contentView?.viewWithTag(4) as! UILabel
        let gendertext = contentView?.viewWithTag(5) as! UILabel
        let agetext = contentView?.viewWithTag(7) as! UILabel
        let pub  = adoptPetss[indexPath.item] as! Dictionary<String,Any>
        let userid = pub["owner"] as! Int
        UserControler.GetUserById(id: "\(userid)", completionHandler:{
            response in
            self.user.user_username = response.user_username
            usernameText.text = "Poster: \(response.user_firstname!) \(response.user_lastname!) "
           // adresstext.text = "Adresse : \( response.user_address! ) "
            let urlImage = "http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"
            profilImage.af_setImage(withURL: URL(string: urlImage)!)

        })
        //let username = self.user.user_username
        let race = pub["pet_race"] as! String
        let age = pub["pet_age"] as! String
        let sex = pub["pet_sex"] as! String
        
        let peturl = "http://127.0.0.1:3000/petrescue/public/img/\(pub["pet_picture"]!)"
        pubImage.af_setImage(withURL: URL(string:peturl)!)
       // petnametext.text = "Pet Name : \(name)"
        breedText.text = "Type : \( race ) "
        gendertext.text = "Gender : \( sex ) "
        agetext.text = "N.Places : \(age ) "
        
        // print(self.idpub)
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "adoptPets", sender: indexPath) //appelle PrepareForSegue automatiquement
    }
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "adoptPets"{
            let destination = segue.destination as! DetailsAdoptPetViewController
            let index = sender as? NSIndexPath
            let serviceshow  = adoptPetss[ index!.item] as! Dictionary<String,Any>
              idpet = (serviceshow["pet_id"] as! Int)
              iduser = (serviceshow["owner"] as! Int)
              destination.idpet  = self.idpet
              destination.iduser = self.iduser
 
        }
    
    }
    

    
 

}
