

import UIKit
import SDWebImage
import BLTNBoard
class NotificationViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    var notiflist:[Notif] = [Notif]()
    var notifController = NotifController()
    let petController = PetController()
    let postController = PostController()
    let adoptionController = AdoptionController()
    let UserDefault = UserDefaults.standard
    let userController = UserController()
    var user = User()
    var post = Post()
    var pet = Pet()
    var index : Int = 0
    var id : Int = 0
    var adopt = Adoption()
    lazy var bulletinManager : BLTNItemManager = {
        let AddPage: BLTNPageItem = BLTNPageItem(title: "Delete notification")
            AddPage.image = UIImage(named: "rejected")
            AddPage.descriptionText = "Do you want to delete the notification"
            AddPage.actionButtonTitle = "Delete"
        AddPage.alternativeButtonTitle = "Cancel"
        
            AddPage.isDismissable = true
        AddPage.actionHandler = {
            (item:BLTNItem) in
            self.notifController.DeleteNotif(id: self.id, completionhandler: {
                response in
                self.notiflist.remove(at: self.index)
                self.titi.reloadData()
                AddPage.manager?.dismissBulletin()
            })
        }
        return BLTNItemManager(rootItem: AddPage)
    }()
        
        lazy var RequestManager : BLTNItemManager = {
            let AddPage: BLTNPageItem = BLTNPageItem(title: "Adoption Request")
                
                
                AddPage.image = UIImage(named: "question")
                AddPage.descriptionText = "An adoption requestion just came , what do we do ?"
                AddPage.actionButtonTitle = "Accept"
            AddPage.alternativeButtonTitle = "Refuse"
            
                AddPage.isDismissable = true
            AddPage.actionHandler = {
                (item:BLTNItem) in
                self.adopt.status = 3
                self.adoptionController.GetAdoptionByPet(Pet: self.adopt.pet!, completionHandler: {
                    response in
                    self.adopt.adoption_id = response.adoption_id!
                    self.adoptionController.UpdateAdoption(adoption: self.adopt, completionHandler: {response in})
                    self.petController.UpdateOwner(owner: response.adoptive!, pet: response.pet!, completionHandler: {response in})
                    self.notifController.AddNotif(notif: Notif(user_id: self.adopt.adoptive!, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: response.pet, post_id: 0, type: "Request Accepted"), completionHandler: {response in})
                                   self.notifController.DeleteNotif(id: self.id, completionhandler: {
                                       response in
                                       AddPage.manager?.dismissBulletin()
                                    self.notiflist.remove(at: self.index)
                                    self.titi.reloadData()
                                   })
                })
               
            }
            
            AddPage.alternativeHandler = {
                (item:BLTNItem) in
                self.adopt.status = 2
                 self.adoptionController.GetAdoptionByPet(Pet: self.adopt.pet!, completionHandler: {
                     response in
                    print(response.adoption_id!)
                    self.adoptionController.DeleteAdoption(post: response.adoption_id!, completionhandler: {response in})
                    self.notifController.AddNotif(notif: Notif(user_id: self.adopt.adoptive, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: response.pet, post_id: 0, type: "Request Rejected"), completionHandler: {response in
                    })
                    self.notifController.DeleteNotif(id: self.id, completionhandler: {
                                                           response in
                                                           AddPage.manager?.dismissBulletin()
                        self.notiflist.remove(at: self.index)
                        self.titi.reloadData()
                                                       })
                                    })
                                  

                
            }
            
            return BLTNItemManager(rootItem: AddPage)
        }()
    
    
    @IBOutlet weak var titi: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return notiflist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifcell")
        let contentView = cell?.viewWithTag(0)
        let userPic = contentView?.viewWithTag(1) as? SDAnimatedImageView
        let notiftypePic = contentView?.viewWithTag(2) as! UIImageView
        let descText = contentView?.viewWithTag(3) as! UITextView
        let dateText = contentView?.viewWithTag(4) as! UILabel
        descText.isEditable = false
        dateText.text = notiflist[indexPath.row].date!
        
        userPic?.layer.borderWidth = 1
        userPic?.layer.masksToBounds = false
        userPic?.layer.borderColor = UIColor.black.cgColor
        userPic?.layer.cornerRadius = (userPic?.frame.height)!/2
        userPic?.clipsToBounds = true
        
        
        userController.GetUserById(id: "\(notiflist[indexPath.row].sender_id!)", completionHandler: {
            response in
            self.user = response
            userPic?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"),placeholderImage: UIImage(named:"profile_round"))
            if ( self.notiflist[indexPath.row].type! == "Like")
            {
                notiftypePic.image = UIImage(named: "like")
                descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just liked your post "
                self.postController.GetPostById(id: self.notiflist[indexPath.row].post_id!, completionHandler: {
                    post in
                    descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just liked your post '\(post.post_title!)' "
                })
                
            }else if (self.notiflist[indexPath.row].type! == "Comment"){
                notiftypePic.image = UIImage(named: "comment-1")
                self.postController.GetPostById(id: self.notiflist[indexPath.row].post_id!, completionHandler: {
                    post in
                    descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just commented on your post '\(post.post_title!)' "
                })
            }
            else if (self.notiflist[indexPath.row].type! == "Adoption Request")
            {
                notiftypePic.image = UIImage(named: "question")
                self.petController.GetPetById(id: self.notiflist[indexPath.row].pet_id!, completionHandler: {
                    pet in
                    
                    descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just requested to join your camping \(pet.pet_name!)"
                    
                    
                })
                
            }else if (self.notiflist[indexPath.row].type!  == "Request Rejected")
            {
                notiftypePic.image = UIImage(named: "rejected")
                self.petController.GetPetById(id: self.notiflist[indexPath.row].pet_id!, completionHandler: {
                    pet in
                    descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just rejected your request to to join his camping \(pet.pet_name!)"
                    
                })
            } else {
                notiftypePic.image = UIImage(named: "yes")
                self.petController.GetPetById(id: self.notiflist[indexPath.row].pet_id!, completionHandler: {
                    pet in
                    descText.text = "\(self.user.user_firstname!) \(self.user.user_lastname!) just Accepted your request to join his camping \(pet.pet_name!) . Enjoy "
                    
                })
            }
        })
        
        //descText.text = notiflist[indexPath.row]
        
        //let desc = notiflist[indexPath.item].foo
        //let date = notiflist[indexPath.item].foo
        //descText.text = ""
        //dateText.text = ""
        return cell!

    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.id = notiflist[indexPath.row].notification_id!
    self.index = indexPath.row
    if(notiflist[indexPath.row].type! != "Adoption Request")
    {
        self.bulletinManager.backgroundViewStyle = .blurredExtraLight
        self.bulletinManager.showBulletin(above: self)
    }else if notiflist[indexPath.row].type! == "Adoption Request"{
        self.adopt.pet = notiflist[indexPath.row].pet_id!
        self.adopt.owner = self.UserDefault.integer(forKey: "user_id")
        self.adopt.adoptive = notiflist[indexPath.row].sender_id!
        self.RequestManager.backgroundViewStyle = .blurredExtraLight
        self.RequestManager.showBulletin(above:self)
    }
   }
    
    override func viewDidLoad() {
        
        
        notifController.GetNotifByUser(user: UserDefault.integer(forKey: "user_id"), completionHandler: {
            response in
            self.notiflist = response
            self.titi.reloadData()
            
        })
        super.viewDidLoad()
        
    }
    

 

}
