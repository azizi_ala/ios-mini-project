

import UIKit
import SDWebImage
class PostDetailViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtCommentaire: UITextField!
    let UserDefault = UserDefaults.standard
    let commentController:CommentController = CommentController()
    let userController:UserController = UserController()
    var post : Post = Post()
    var pvc : PostsViewController!
    var list : [Comment] = [Comment]()
    var notifController = NotifController()
    
    override func viewDidLoad() {
        commentController.getCommentsByPost(id: post.post_id!, completionhandler: {
            response in
            self.list = response
            self.tableView.reloadData()
        })
       super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Commentaires")
        
        let contentView = cell?.viewWithTag(0)
        let nom = contentView?.viewWithTag(1) as! UILabel
        let serviceDate = contentView?.viewWithTag(2) as! UILabel
        let serviceCom = contentView?.viewWithTag(3) as! UITextView
        let userpic = contentView?.viewWithTag(4) as? SDAnimatedImageView
        let delete = contentView?.viewWithTag(10) as? UIImageView
        userpic?.layer.borderWidth = 1
        userpic?.layer.masksToBounds = false
        userpic?.layer.borderColor = UIColor.black.cgColor
        userpic?.layer.cornerRadius = (userpic?.frame.height)!/2
        userpic?.clipsToBounds = true
        userController.GetUserById(id: "\(list[indexPath.row].user!)", completionHandler: {
            response in
            nom.text = "\(response.user_firstname!) " + "\(response.user_lastname!)"
        userpic?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"),placeholderImage: UIImage(named:"profile_round"))
        })
        serviceDate.text = list[indexPath.row].comment_date!
        serviceCom.text = list[indexPath.row].body!
        let tapdelete = MyTapGesture.init(target: self,action : #selector(PostDetailViewController.deleteComment))
        tapdelete.comment = list[indexPath.row].comment_id!
        tapdelete.index = indexPath.row
        delete?.addGestureRecognizer(tapdelete)
        delete?.isUserInteractionEnabled = true
        if(list[indexPath.row].user! == UserDefault.integer(forKey: "user_id"))
        {
            delete?.isHidden = false
        }
        
        return cell!
    }
    
    @objc func deleteComment(recognizer : MyTapGesture)
    {
        commentController.DeleteComment(comment: recognizer.comment, completionhandler: {
            response in
            self.list.remove(at: recognizer.index)
            self.tableView.reloadData()
        })
        
    }
    
    @IBAction func RetourBt(_ sender: Any) {
        pvc.titi.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func AddCmt(_ sender : Any)
    {
        commentController.AddComment(post: post.post_id!, user: UserDefault.integer(forKey: "user_id"), body: txtCommentaire.text!, completionhandler: {
            response in
            self.commentController.getCommentsByPost(id: self.post.post_id!, completionhandler: {
                response in
                self.notifController.AddNotif(notif: Notif(user_id: self.post.post_user!, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: 0, post_id: self.post.post_id!, type: "Comment"), completionHandler: {i in })
                let content = UNMutableNotificationContent()
                content.title = "New Comment !"
                content.body = "\(self.UserDefault.string(forKey: "user_firstname")!) \(self.UserDefault.string(forKey: "user_lastname")!) just commented on your post !"
                content.sound = UNNotificationSound.default
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                       let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
                       
                       UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                self.list = response
                self.tableView.reloadData()
            })
        })
    }
    
       
}
