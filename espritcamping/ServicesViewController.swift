

import UIKit

class ServicesViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate {
    
    var images = ["Mountain","Oasis","Beach"]
    var titres = ["My Camping","Open Camping","Private Camping"]
    var descriptions = ["Here You can see your Camping informations , remove or update it " ,"Here you can search for Open Camping", "Here you can search for private camping and get more information about it " ]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellService", for: indexPath)
        let contentView = cell.viewWithTag(0)
        let img = contentView?.viewWithTag(1) as! UIImageView
        let titre = contentView?.viewWithTag(2) as! UILabel
        let description = contentView?.viewWithTag(3) as! UITextView
        img.image = UIImage(named: images[indexPath.row])
        titre.text = titres[indexPath.row]
        description.text=descriptions[indexPath.row]
        description.isEditable = false
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true;
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width:0,height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        
        return cell}
    //adoptPets
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(images[indexPath.row] == "Mountain" ) {
            performSegue(withIdentifier: "petforcare", sender: indexPath)
        }
        else if (images[indexPath.row] == "Oasis" ){
            performSegue(withIdentifier: "lostPets", sender: indexPath)
        } else if (images[indexPath.row] == "Beach" ){
            performSegue(withIdentifier: "adoptPets", sender: indexPath)
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "petforcare"{
            
            //  self.seguePetForCare()
            //  let destination = segue.destination as! DetailView
            //let indice = sender as! IndexPath
            // destination.nomImage = images[indice.row]
        }
        
        
    }
    
    func seguePetForCare(){
        self.performSegue(withIdentifier: "petforcare", sender: self)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //passageLabel.text! = message!
        
        // Do any additional setup after loading the view.
    }
    
    
  
    
}
