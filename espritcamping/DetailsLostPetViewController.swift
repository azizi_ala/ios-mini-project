
import UIKit
import Alamofire
import AlamofireImage

class DetailsLostPetViewController: UIViewController {
     //Outlets
    //@IBOutlet weak var photouser: ImageRonde!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var photopet: UIImageView!
    @IBOutlet weak var breedText: UILabel!
    @IBOutlet weak var genderText: UILabel!
    @IBOutlet weak var ageText: UILabel!
    @IBOutlet weak var usernamedetails: UILabel!
    @IBOutlet weak var emailText: UILabel!
    @IBOutlet weak var adresseText: UILabel!
    @IBOutlet weak var phoneText: UILabel!
    
    @IBOutlet weak var userfullname: UILabel!
    //Utils
    var idpet:Int = 0
    var iduser:Int = 0
    var petController = PetController()
    var userController = UserController()
    
    @IBAction func BtBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
         FetchData()
    }
    
    func FetchData() {
        // print(self.idpet)
        petController.GetPetById(id: idpet, completionHandler: {
         response in
                //self.usernameText.text =
               // self.datePub.text = (jsonData.value(forKey: "datepub")as! String)
            self.breedText.text = response.pet_race
            self.genderText.text = response.pet_sex
            self.ageText.text = response.pet_age
            self.descriptionText.text = response.pet_desc
            self.descriptionText.isEditable = false
           // self.ageText.text = (response.pet_age as! String)
              //  self.usernamedetails.text = (jsonData.value(forKey: "username")as! String)
               // self.adresseText.text = (jsonData.value(forKey: "address")as! String)
               // self.primeText.text = "Reward : \(jsonData.value(forKey: "price")as! String) "
               // self.phoneText.text = (jsonData.value(forKey: "phone")as! String)
              //  self.emailText.text = (jsonData.value(forKey: "email")as! String)
              //  self.MissingCity.text = (jsonData.value(forKey: "MissingCity")as! String)
                
              //  self.descriptionText.text = (jsonData.value(forKey: "descrip")as! String)
            let urlImage = "http://127.0.0.1:3000/petrescue/public/img/\(response.pet_picture!)"
                
            self.photopet.af_setImage(withURL:URL(string: urlImage)!)
            
                
        })
        userController.GetUserById(id: "\(iduser)", completionHandler: {
        response in
        self.adresseText.text = response.user_address
        self.phoneText.text = response.user_phonenumber
        self.emailText.text = response.user_email
        self.userfullname.text = "\(response.user_firstname!)" + " \(response.user_lastname!)"
            // self.MissingCity.text = response.user_address
           // let urlProfilImage = "http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"
            //self.photouser.af_setImage(withURL:URL(string: urlProfilImage)!)
            
            
        })
        
    }}
	
