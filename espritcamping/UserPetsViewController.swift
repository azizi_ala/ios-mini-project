

import UIKit
import Alamofire
import AlamofireImage


class UserPetsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var list: [Pet] = [Pet]()
    @IBOutlet weak var tableV: UITableView!
    var idpet : String = ""
    let UserDefault = UserDefaults.standard
    var petController = PetController()
    var pet_idd: Int = 0
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    @IBAction func btBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Searchps")
        
        let contentView = cell?.viewWithTag(0)
        let pubImage = contentView?.viewWithTag(1) as! UIImageView
        let descText = contentView?.viewWithTag(2) as! UITextView
        let petnameText = contentView?.viewWithTag(3) as! UILabel
        let peracetext = contentView?.viewWithTag(4) as! UILabel
        let agetext = contentView?.viewWithTag(5) as! UILabel
        let gendertext = contentView?.viewWithTag(7) as! UILabel
        let delete = contentView?.viewWithTag(8) as! UIButton
        let petname = list[indexPath.item].pet_name!
        let petdesc = list[indexPath.item].pet_desc!
        let petrace = list[indexPath.item].pet_race!
        let petage = list[indexPath.item].pet_age!
        let petgender = list[indexPath.item].pet_sex!
        let pet_id = list[indexPath.item].pet_id!
        self.pet_idd = pet_id
        let peturl = "http://127.0.0.1:3000/petrescue/public/img/\(list[indexPath.item].pet_picture!)"
        pubImage.af_setImage(withURL: URL(string:peturl)!)
      //  let urlImage = Connexion.adresse +  "images/" + ( pub["petimg"] as! String )
       // let urlProfilImage = Connexion.adresse +  "images/" + ( pub["photo"] as! String )
        
        //      print(pub["img"])
        
       // profilImage.af_setImage(withURL:URL(string: urlProfilImage)!)
      //  pubImage.af_setImage(withURL:URL(string: urlImage)!)
        
        descText.text = petdesc
        descText.isEditable = false
        petnameText.text = "Name : \( petname ) "
        peracetext.text = "Type : \( petrace ) "
        agetext.text = "Nb Places : \( petage ) "
        gendertext.text = "Gender : \(petgender ) "
        
        
        let tapdelete = MyTapGesture.init(target: self,action : #selector(UserPetsViewController.btn_rmpet))
        tapdelete.pet_id = list[indexPath.row].pet_id!
        tapdelete.index = indexPath.row
        delete.addGestureRecognizer(tapdelete)
        delete.isUserInteractionEnabled = true
        return cell!
    }
    
    
    
    override func viewDidLoad() {
        petController.getPetsByOwner(id : UserDefault.integer(forKey: "user_id"),completionhandler:
            {
                response in
                self.list = response
                self.tableV.reloadData() })
          super.viewDidLoad()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "SPSdetails", sender: indexPath) //appelle PrepareForSegue automatiquement
    }
    
    @objc func btn_rmpet(recognizer : MyTapGesture) {
        petController.DeletePet(id: recognizer.pet_id, completionhandler:{
            response in
            self.list.remove(at: recognizer.index)
            self.tableV.reloadData()
        })
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toedit"{
            let destination = segue.destination as! UpdateViewController
            destination.petid = pet_idd
            
        }
        
    }
    
    

    

}

class MyTapGesture : UITapGestureRecognizer {
    var pet_id : Int = 0
    var pet_name : String = ""
    var pet_race : String = ""
    var pet_age : String = ""
    var pet_status : String = ""
    var pet_picture : String = ""
    var pet_sex : String = ""
    var pet_desc: String = ""
    var owner: Int = 0
    var index: Int = 0
    var post : Int = 0
    var comment : Int = 0
    var user : Int = 0
}
