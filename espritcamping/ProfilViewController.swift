

import UIKit
import Alamofire
import AlamofireImage
import BLTNBoard
import SDWebImage
class ProfilViewController: UIViewController,UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {
    
    //IBoutlet
    @IBOutlet weak var labelUpdate: UILabel!
    @IBOutlet weak var buttonUpdate: UIImageView!
    @IBOutlet weak var LogOut: UILabel!
    @IBOutlet weak var buttonOut: UIImageView!
    @IBOutlet weak var petCount: UILabel!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var ImageUser: SDAnimatedImageView?
    @IBOutlet weak var Username: UILabel!
    @IBOutlet weak var EmailUser: UILabel!
    @IBOutlet weak var AdresseUser: UILabel!
    @IBOutlet weak var PhoneUser: UILabel!
    let petController : PetController = PetController()
    let postController : PostController = PostController()
    lazy var bulletinManager : BLTNItemManager = {
        let AddPage: BLTNPageItem = BLTNPageItem(title: "User update")
            
            
            AddPage.image = UIImage(named: "rectangle.stack.person.crop.fill")
            AddPage.descriptionText = "In order to enjoy our app , we will redirect you to your profile so you can fill more informations"
            AddPage.isDismissable = false
            AddPage.alternativeButtonTitle = "Continue"
        return BLTNItemManager(rootItem: AddPage)
    }()
        
        //others
        let UserDefault = UserDefaults.standard
    
        //let url_profile = Connexion.adresse +  "user/GetUser.php"
        var profils : NSArray = []
    var status: String = ""
        
        
        override func viewDidLoad() {super.viewDidLoad()
            UserDefault.synchronize()
            FetchData()
            self.Username.text = self.UserDefault.string(forKey: "user_username")!
            self.EmailUser.text = self.UserDefault.string(forKey: "user_email")!
            self.PhoneUser.text = String(self.UserDefault.string(forKey: "user_phonenumber")!)
            self.AdresseUser.text = String(self.UserDefault.string(forKey: "user_address")!)
            //nationalite
            //"adresse"
            
            self.ImageUser?.layer.borderWidth = 1
            self.ImageUser?.layer.masksToBounds = false
            self.ImageUser?.layer.borderColor = UIColor.black.cgColor
            self.ImageUser?.layer.cornerRadius = (self.ImageUser?.frame.height)!/2
            self.ImageUser?.clipsToBounds = true
            self.ImageUser?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(UserDefault.string(forKey: "user_picture")!)"),placeholderImage: UIImage(named:"profile_round"))
           // self.ImageUser.layer.borderWidth = 2
            //self.ImageUser.layer.borderColor = UIColor.black.cgColor
            self.ImageUser?.sd_imageIndicator = SDWebImageProgressIndicator.default
            let tapOut = MyTapGesture.init(target: self, action: #selector(ProfilViewController.deconnexion))
            self.LogOut.addGestureRecognizer(tapOut)
            self.LogOut.isUserInteractionEnabled = true
            let tapOut2 = MyTapGesture.init(target: self, action: #selector(ProfilViewController.toDetail))
            self.labelUpdate.addGestureRecognizer(tapOut2)
            self.labelUpdate.isUserInteractionEnabled = true
            /*self.buttonOut.addGestureRecognizer(tapOut2)
            self.buttonOut.isUserInteractionEnabled = true*/
            
        }
    
    @objc func toDetail(recognizer : MyTapGesture)
    {
        performSegue(withIdentifier: "editProfil", sender: nil)
    }
    
   
    @objc func deconnexion(recognizer : MyTapGesture) {
            UserDefault.removeObject(forKey: "user_id")
            UserDefault.removeObject(forKey: "user_username")
            UserDefault.removeObject(forKey: "user_phonenumber")
            UserDefault.removeObject(forKey: "user_email")
            UserDefault.removeObject(forKey: "user-address")
            UserDefault.removeObject(forKey: "user_lastname")
            UserDefault.removeObject(forKey: "user_firstname")
            UserDefault.removeObject(forKey: "signup")
            UserDefault.removeObject(forKey: "user_id")
           // UserDefault.removeObject(forKey: "login")
            //if UserDefault.string(forKey: "login") == nil {
                let next = self.storyboard!.instantiateViewController(withIdentifier: "LoginView")
                self.present(next, animated: true, completion: nil)
                
          //  }
            
            print("User Logged Out")
        }
        
        
        
        
        func FetchData() {
            let UserDefault = UserDefaults.standard
            postController.GetPostByOwner(id: UserDefault.integer(forKey: "user_id"), completionHandler: {
                response in
                self.postCount.text = "\(response.count)"
            })
    }
        
        
   
}

