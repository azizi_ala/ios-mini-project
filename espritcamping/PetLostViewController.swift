

import UIKit
import Alamofire
import AlamofireImage

class PetLostViewController: UIViewController  , UITableViewDelegate, UITableViewDataSource {
    let URL_ALL_PetLost = "http://127.0.0.1:3000/petrescue/pets/status/Lost"
    let URL_UserById = "http://127.0.0.1:3000/petrescue/"
    @IBOutlet weak var tableV: UITableView!
    var petLosts : NSArray = []
     var idpet:Int = 0
    var UserControler = UserController()
    var user = User()
    var iduser:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request(URL_ALL_PetLost).responseJSON
            {
                response in
                self.petLosts = response.result.value as! NSArray
                self.tableV.reloadData()
        }
    }
    
   
    @IBAction func BtBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return petLosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "petLost")
        
        let contentView = cell?.viewWithTag(0)
         let profilImage = contentView?.viewWithTag(1) as! UIImageView
        let pubImage = contentView?.viewWithTag(2) as! UIImageView
        let usernameText = contentView?.viewWithTag(3) as! UILabel
        let breedText = contentView?.viewWithTag(4) as! UILabel
        let adresstext = contentView?.viewWithTag(5) as! UILabel
        let agetext = contentView?.viewWithTag(6) as! UILabel
        let pricetext = contentView?.viewWithTag(7) as! UILabel
        
        
        let pub  = petLosts[indexPath.item] as! Dictionary<String,Any>
        let userid = pub["owner"] as! Int
        UserControler.GetUserById(id: "\(userid)", completionHandler:{
            response in
            self.user.user_username = response.user_username

            usernameText.text = "Poster: \(response.user_firstname!)  \(response.user_lastname!) "
            let urlImage = "http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"
            profilImage.af_setImage(withURL: URL(string: urlImage)!)
            print(response.user_username!)
        })
        let peturl = "http://127.0.0.1:3000/petrescue/public/img/\(pub["pet_picture"]!)"
        pubImage.af_setImage(withURL: URL(string:peturl)!)
        //let username = self.user.user_username
        let name = pub["pet_name"] as! String
        let race = pub["pet_race"] as! String
        let age = pub["pet_age"] as! String
        let sex = pub["pet_sex"] as! String
       
        
       
    
        //      print(pub["img"])
        
       
        pubImage.image = UIImage(named:"Oasis")
        breedText.text = "Name : \( name ) "
        adresstext.text = "Type : \( race ) "
        pricetext.text = "Gender : \( sex ) "
        agetext.text = "N.Places : \(age ) "
        
       
        return cell!
    }
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "lostPetsDetails", sender: indexPath) //appelle PrepareForSegue automatiquement
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "lostPetsDetails"{
            let destination = segue.destination as! DetailsLostPetViewController
            let index = sender as? NSIndexPath
            let serviceshow  = petLosts [ index!.item] as! Dictionary<String,Any>
             idpet = (serviceshow["pet_id"] as! Int)
            iduser = (serviceshow["owner"] as! Int)
            destination.idpet  = self.idpet
            destination.iduser = self.iduser 
            
            
        }
    }
}
