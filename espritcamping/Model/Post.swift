
import Foundation
import SwiftyJSON
import ObjectMapper
class Post:Mappable{
   
    var post_id : Int?
    var post_title : String?
    var post_description : String?
    var post_date : String?
    var post_picture : String?
    var post_user : Int?
    var post_localisation : String?
    
    required init?(map: Map) {
           
       }
    init(title:String,description:String,picture:String,user:Int)
    {
        self.post_user = user
        self.post_description=description
        self.post_title=title
        self.post_picture=picture
    }
       
       func mapping(map: Map) {
            post_id <- map["post_id"]
            post_title <- map["post_title"]
            post_localisation <- map["post_localisation"]
            post_date <- map["post_date"]
            post_user <- map["post_user"]
            post_picture <- map["post_image"]
            post_description <- map["post_description"]
       }
    
    init(json:JSON){
        post_id = json["post_id"].intValue
        post_user = json["post_user"].intValue
        post_picture = json["post_image"].stringValue
        post_date = json["post_date"].stringValue
        post_description = json["post_description"].stringValue
        post_title = json["post_title"].stringValue
        post_localisation = json["post_localisation"].stringValue
    }
       
    
    init()
    {
        post_title = "Empty"
    }
    
    init(id:Int?,title:String?,description:String?,date:String?,picture:String?,user:Int?,local:String?)
    {
        post_id = id
        post_title = title
        post_description = description
        post_date = date
        post_picture = picture
        post_user = user
        post_localisation=local
    }
    init(title:String?,description:String?,date:String?,picture:String?,user:Int?,local:String?)
    {
        post_title = title
        post_description = description
        post_date = date
        post_picture = picture
        post_user = user
        post_localisation=local
    }
    
}
