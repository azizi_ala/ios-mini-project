
import Foundation
import SwiftyJSON
import ObjectMapper

class Pet: Mappable{
    var pet_id : Int?
    var pet_name : String?
    var pet_race : String?
    var pet_age : String?
    var pet_status : String?
    var pet_picture : String?
    var pet_sex : String?
    var pet_desc: String?
    var owner: Int?
    
    
    init(json:Any)
    {
        
    }
    init()
      {
        pet_name = ""
        pet_age = ""
        pet_desc = ""
        pet_picture = ""
        pet_status = ""
        pet_sex = ""
          
      }
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        pet_id <- map["pet_id"]
        pet_name <- map["pet_name"]
        pet_race <- map["pet_race"]
        pet_age <- map["pet_age"]
        pet_status <- map["pet_status"]
        pet_picture <- map["pet_picture"]
        pet_sex <- map["pet_sex"]
        pet_desc <- map["pet_desc"]
        owner <- map["owner"]
    }
     init(json:JSON)
       {
            self.pet_id = json["pet_id"].intValue
            self.pet_name = json["pet_name"].stringValue
            self.pet_race = json["pet_race"].stringValue
            self.pet_age = json["pet_age"].stringValue
            self.pet_status = json["pet_status"].stringValue
            self.pet_picture = json["pet_picture"].stringValue
            self.pet_sex = json["pet_sex"].stringValue
            self.pet_desc = json["pet_desc"].stringValue
            self.owner = json["owner"].intValue
       }
    init(name:String?,desc:String?,race:String?,age:String?,picture:String?,sex:String?,status:String?,owner:Int?)
    {
        pet_age = age
        pet_status = status
        pet_desc = desc
        pet_name = name
        pet_race = race
        pet_picture = picture
        pet_sex = sex
        pet_desc = desc
        self.owner = owner
    }
    init(id: Int?, name: String?, race: String?, age: String?, picture: String?, sex: String?, desc: String?,status: String?, owner: Int?)
    {
        pet_id = id
        pet_age = age
        pet_desc = desc
        pet_name = name
        pet_race = race
        pet_picture = picture
        pet_sex = sex
         pet_status = status
        pet_desc = desc
        self.owner = owner
    }
    init(id:Int?,name:String?,race:String?,age:String?,picture:String?,sex:String?,owner:Int?)
    {
        pet_age = age
        pet_name = name
        pet_race = race
        pet_picture = picture
        self.owner = owner
        pet_id = id
        pet_sex = sex
    }
    
    init(name:String?,race:String?,age:String?,picture:String?,sex:String?)
    {
        pet_age = age
        pet_name = name
        pet_race = race
        pet_picture = picture
        pet_sex = sex
        
    }
}
