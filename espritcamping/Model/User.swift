

import Foundation
import SwiftyJSON
import ObjectMapper

class User :Mappable{

    var user_id : Int?
    var user_username: String?
    var user_email: String?
    var password: String?
    var user_firstname: String?
    var user_lastname: String?
    var user_picture: String?
    var user_address: String?
    var user_phonenumber: String?
    var roles: String?
    var salt: Int?

    init(json: Any)
    {
    }
    required init(map: Map) {
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        user_username <- map["user_username"]
        user_email <- map["user_email"]
        password <- map["password"]
        user_address <- map["user_address"]
        user_firstname <- map["user_firstname"]
        user_lastname <- map["user_lstname"]
        user_picture <- map["user_picture"]
        user_phonenumber <- map["user_phonenumber"]
        roles <- map["roles"]
    }
    init(){
        
    }
    init(email:String?,username:String?,pwd:String?,phonenumber:String?)
    {
        user_phonenumber = phonenumber
        user_username = username
        user_email = email
        password = pwd
    }

    init(json : JSON){
        
        user_id = json["user_id"].intValue
        user_email = json["user_email"].stringValue
        user_username = json["user_username"].stringValue
        password = json["password"].stringValue
        user_firstname = json["user_firstname"].stringValue
        user_lastname = json["user_lastname"].stringValue
        user_picture = json["user_picture"].stringValue
        user_address = json["user_address"].stringValue
        user_phonenumber = json["user_phonenumber"].stringValue
        roles = json["roles"].stringValue
        salt = json["salt"].intValue
        
    }
    
    init(id: Int?,email:String?,username:String?,pwd:String?,firstname:String?,lastname:String?,picture:String?,address:String?,phonenumber:String?)
    {
        user_id = id
        user_email = email
        user_username = username
        password = pwd
        user_firstname = firstname
        user_lastname = lastname
        user_picture = picture
        user_address = address
        user_phonenumber = phonenumber
    }
    
    
    
    /*func toString()
    {
        print( "User{" +
            "user_id=" + self.user_id +
            ", user_username=" + self.user_username +
            ", user_email=" + self.user_email +
            ", password=" + self.password +
            ", user_firstname=" + self.user_firstname +
            ", user_lastname=" + self.user_lastname +
            ", user_picture=" + self.user_picture +
            ", user_address=" + self.user_address +
            ", user_phonenumber=" + self.user_phonenumber)
    }*/
    

}
