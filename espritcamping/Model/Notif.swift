

import Foundation
import SwiftyJSON
import ObjectMapper

class Notif: Mappable {
    
    
    var notification_id:Int?
    var user_id:Int?
    var sender_id:Int?
    var pet_id:Int?
    var post_id:Int?
    var type:String?
    var date:String?
    //initia
    init(){}
    init(json:Any){}
    required init?(map: Map) {}
    
    func mapping(map: Map)
    {
           notification_id <- map["notification_id"]
           user_id <- map["user_id"]
           sender_id <- map["sender_id"]
           pet_id <- map["pet_id"]
           post_id <- map["post_id"]
           type <- map["type"]
           date <- map["date"]
       }
    //constractors
    
    init(notification_id:Int?, user_id:Int?, sender_id:Int? , pet_id:Int?,post_id:Int?,type:String? , date:String?){
        
        self.notification_id = notification_id
        self.user_id = user_id
        self.sender_id = sender_id
        self.pet_id = pet_id
        self.post_id = post_id
        self.type = type
        self.date = date
        
    }
    init(user_id:Int?, sender_id:Int? , pet_id:Int?,post_id:Int?,type:String? ){
          self.user_id = user_id
          self.sender_id = sender_id
          self.pet_id = pet_id
          self.post_id = post_id
          self.type = type
          
      }
    
    
    
}
