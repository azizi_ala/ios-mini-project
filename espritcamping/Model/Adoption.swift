
import Foundation
import SwiftyJSON
import ObjectMapper

class Adoption {
    var  adoption_id : Int?
    var  owner : Int?
    var  adoptive : Int?
    var  pet : Int?
    var  status : Int?
    
    init(){}
    init(json:Any){}
    
    func mapping(map: Map)
    {
           adoption_id <- map["adoption_id"]
           adoptive <- map["adoptive"]
           pet <- map["pet"]
           status <- map["status"]
           owner <- map["owner"]
       }
    init(json:JSON)
    {
         self.adoption_id = json["adoption_id"].intValue
         self.adoptive = json["adoptive"].intValue
         self.pet = json["pet"].intValue
         self.status = json["status"].intValue
         self.owner = json["owner"].intValue
    }
    init(owner:Int?,adoptive:Int?,pet:Int?,status:Int?)
    {
        self.owner = owner
        self.adoptive = adoptive
        self.pet = pet
        self.status = status
    }
    
    init(adoption_id:Int?,owner:Int?,adoptive:Int?,pet:Int?,status:Int?)
    {
           self.adoption_id = adoption_id
           self.owner = owner
           self.adoptive = adoptive
           self.pet = pet
           self.status = status
       }
    
}
