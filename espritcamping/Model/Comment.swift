

import Foundation
import SwiftyJSON
import ObjectMapper
class Comment :Mappable{
    var comment_id : Int?
    var body : String?
    var user : Int?
    var post : Int?
    var comment_date : String?
    
    init(json:Any)
    {
        
    }
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        comment_id <- map["comment_id"]
        body <- map["body"]
        user <- map["user"]
        post <- map["post"]
        comment_date <- map["comment_date"]
    }
    
    init(comment_id:Int?,post:Int?,user:Int?,body:String?,comment_date:String?)
    {
        self.comment_date = comment_date
        self.body = body
        self.user = user
        self.post = post
        self.comment_id = comment_id
    }
    
    init(post:Int?,user:Int?,body:String?)
    {
        self.post = post
        self.user = user
        self.body = body
    }
    
    init(json:JSON)
    {
        self.comment_id = json["comment_id"].intValue
        self.user = json["user"].intValue
        self.post = json["post"].intValue
        self.body = json["body"].stringValue
        self.comment_date = json["comment_date"].stringValue
    }
}
