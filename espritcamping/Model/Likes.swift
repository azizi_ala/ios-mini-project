

import Foundation
import SwiftyJSON
import ObjectMapper

class Likes : Mappable{
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.likes_id <- map["likes_id"]
        self.user <- map["user"]
        self.post <- map["post"]
    }
    
    init(){}
    
    init(id:Int?,post:Int?,user:Int?)
    {
        self.likes_id = id
        self.user=user
        self.post=post
    }
    
    init(post:Int?,user:Int?)
    {
        self.user=user
        self.post=post
    }
    
    init(json: JSON)
    {
        self.user = json["user"].intValue
        self.post = json["post"].intValue
        self.likes_id = json["likes_id"].intValue
    }
    
    var likes_id:Int?
    var user : Int?
    var post: Int?
    
}
