

import UIKit
import SDWebImage
import Floaty 
import BLTNBoard
import BadgeHub
class PostsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIViewControllerTransitioningDelegate {
    
    var postController:PostController = PostController()
    var userController:UserController = UserController()
    var likesController:LikeController = LikeController()
    var commentController:CommentController = CommentController()
    var profil:ProfilViewController = ProfilViewController()
    var list:[Post] = []
    let UserDefault = UserDefaults.standard
    let floaty :Floaty = Floaty()
    var titre:UITextField!
    var desc:UITextField!
    var nom_image : String!
    var addAlert: UIAlertController!
    var confirm: UIAlertController!
    var notifController = NotifController()
    var BulletinTitle:String! = ""
    var BulletinDescription:String! = ""
    var hub:BadgeHub = BadgeHub(string : "")
    var hubCom:BadgeHub = BadgeHub(string : "")
    lazy var bulletinManager : BLTNItemManager = {
        let AddPage: BLTNPageItem = BLTNPageItem(title: "Must update informations")
            
            
            AddPage.image = UIImage(named: "rectangle.stack.person.crop.fill")
            AddPage.descriptionText = "In order to enjoy our app , you must update your profile so you can fill more informations"
            AddPage.isDismissable = true
            
                    /*   if  let conVC = storyBoard.instantiateViewController(withIdentifier: "Profil") as? ProfilViewController,
                           let navController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController{
                         navController.pushViewController(conVC, animated: true)
            }*/
               
            
        
        return BLTNItemManager(rootItem: AddPage)
    }()
    /*let titles = ["My kitty <3","Dog For Adoption","Starving Animals"]
    let dcs = ["8/11/2019","8/11/2019","8/11/2019"]
    let imagesPosts = ["1","2","1"]*/
    
    
    @IBOutlet weak var titi: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celAct" )
        let contentView = cell?.viewWithTag(0)
        let username = contentView?.viewWithTag(2) as! UILabel
        let datepost = contentView?.viewWithTag(5) as! UILabel
        let description = contentView?.viewWithTag(3)as! UITextView
        let like = contentView?.viewWithTag(6)as! UIImageView
        let like_count = contentView?.viewWithTag(7)as! UILabel
        let com = contentView?.viewWithTag(8)as! UIImageView
        let com_count = contentView?.viewWithTag(9)as! UILabel
        let userpic = contentView?.viewWithTag(4) as? SDAnimatedImageView
        let imgPost = contentView?.viewWithTag(1) as? SDAnimatedImageView
        let delete = contentView?.viewWithTag(10) as? UIImageView
        let title = contentView?.viewWithTag(11) as! UILabel
        title.text = list[indexPath.row].post_title!
        contentView?.layer.borderWidth = 2
        contentView?.layer.masksToBounds = false
        contentView?.layer.borderColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        contentView?.layer.backgroundColor = UIColor.init(red: 27, green: 160, blue: 148).cgColor
        //imgPost?.layer.borderWidth = 1
        //imgPost?.layer.masksToBounds = false
        //imgPost?.layer.borderColor = UIColor.black.cgColor
        userpic?.layer.borderWidth = 1
        userpic?.layer.masksToBounds = false
        userpic?.layer.borderColor = UIColor.black.cgColor
        userpic?.layer.cornerRadius = (userpic?.frame.height)!/2
        userpic?.clipsToBounds = true
        datepost.text = list[indexPath.row].post_date!
        description.text = list[indexPath.row].post_description!
        
        
        imgPost?.sd_imageIndicator = SDWebImageProgressIndicator.default
        imgPost?.isHidden = false
        //hub = BadgeHub(view: like_count)
        //hubCom = BadgeHub(view : com_count)
        if(list[indexPath.row].post_picture! != "empty") {
    imgPost?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(list[indexPath.row].post_picture!)"),placeholderImage: UIImage(named:"2"))
        }else{
            imgPost?.isHidden=true
        }
        userpic?.sd_imageIndicator = SDWebImageProgressIndicator.default
        userController.GetUserById(id: "\(list[indexPath.row].post_user!)", completionHandler: {
            response in
            username.text = "\(response.user_firstname!)  \(response.user_lastname!)"
            userpic?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"),placeholderImage: UIImage(named:"profile_round"))
            print()
        })
        
        
        likesController.getLikesByPost(id: list[indexPath.row].post_id!, completionhandler:{   response in
            like_count.text = "\(response.count) Likes"
            /*self.hub.setCount(response.count)
            self.hub.pop()
            self.hub.blink()*/
            
            })
        commentController.getCommentsByPost(id: list[indexPath.row].post_id!, completionhandler: {
            response in
            com_count.text = "\(response.count) Comments"
           /* self.hubCom.setCount(response.count)
            self.hubCom.pop()
            self.hubCom.blink()*/
        })
        let tapped = MyTapGesture.init(target : self ,action: #selector(PostsViewController.LikeClick))
        tapped.post = list[indexPath.row].post_id!
        tapped.user = list[indexPath.row].post_user!
       // let Tap = UITapGestureRecognizer(target: self, action: #selector(PostsViewController.LikeClick(post : 1)))
        like.addGestureRecognizer(tapped)
        like.isUserInteractionEnabled = true
        
        let tap = MyTapGesture.init(target: self, action: #selector(PostsViewController.goToComments))
        tap.index = indexPath.row
        com.addGestureRecognizer(tap)
        com.isUserInteractionEnabled = true
        
        
        let tapdelete = MyTapGesture.init(target: self,action : #selector(PostsViewController.deletePost))
        tapdelete.post = list[indexPath.row].post_id!
        tapdelete.index = indexPath.row
        delete?.addGestureRecognizer(tapdelete)
        delete?.isUserInteractionEnabled = true
        if(list[indexPath.row].post_user! == UserDefault.integer(forKey: "user_id"))
        {
            delete?.isHidden = false
        }
        
        
        
        
       return cell!
    }
    
    @objc func deletePost(recognizer : MyTapGesture)
    {
        postController.DeletePost(post: recognizer.post, completionhandler: {
            response in
            self.list.remove(at: recognizer.index)
            self.titi.reloadData()
        })
    }
    
    @objc func goToComments(recognizer : MyTapGesture)
    {
        if(UserDefault.integer(forKey: "signup") != 1){
        performSegue(withIdentifier: "toPostDetail", sender: recognizer.index)
            }else{
                bulletinManager.backgroundViewStyle = .blurredExtraLight
                bulletinManager.showBulletin(above: self)
            }
    }

    @objc func LikeClick(recognizer : MyTapGesture)
    {
        if(UserDefault.integer(forKey: "signup") != 1){
        let i:Int = UserDefaults.standard.integer(forKey: "user_id")
        likesController.getLikesByPost(id: recognizer.post, completionhandler: {
            response in
            if(response.count > 0)
            {
                for j in response {
                    
                    
                    if(j.user! == i )
                    {
                        self.likesController.Dislike(post: recognizer.post, user: i, completionhandler: {
                            response in
                            /*self.hub.decrement()
                            self.hub.pop()
                            self.hub.blink()*/
                            self.titi.reloadData()
                        })
                        
                    }else{
                        self.likesController.AddLike(post: recognizer.post, user:UserDefaults.standard.integer(forKey: "user_id"), completionhandler: {
                            response in
                            let content = UNMutableNotificationContent()
                            content.title = "New like !"
                            content.body = "\(self.UserDefault.string(forKey: "user_firstname")!) \(self.UserDefault.string(forKey: "user_lastname")!) just liked your post !"
                            content.sound = UNNotificationSound.default
                            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                                   let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
                            self.notifController.AddNotif(notif: Notif(user_id: recognizer.user, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: 0, post_id: recognizer.post, type: "Like"), completionHandler: {i in })
                                 /*  self.hub.increment()
                                   self.hub.pop()
                                   self.hub.blink()*/
                                   UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                            self.titi.reloadData()
                        })
                    }
                    
                }
            }else{
                self.likesController.AddLike(post: recognizer.post, user:UserDefaults.standard.integer(forKey: "user_id"), completionhandler: {
                    response in
                    
                    
                    self.notifController.AddNotif(notif: Notif(user_id: recognizer.user, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: 0, post_id: recognizer.post, type: "Like"), completionHandler: {i in })
                    let content = UNMutableNotificationContent()
                    content.title = "New like !"
                    content.body = "\(self.UserDefault.string(forKey: "user_firstname")!) \(self.UserDefault.string(forKey: "user_lastname")!) just liked your post !"
                    content.sound = UNNotificationSound.default
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                           let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
                           
                           UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                   // self.hub.increment()
                   // self.hub.pop()
                   // self.hub.blink()
                    self.titi.reloadData()
                })
            }
            
        })
        }else{
            bulletinManager.backgroundViewStyle = .blurredExtraLight
            bulletinManager.showBulletin(above: self)
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //performSegue(withIdentifier: "toPostDetail", sender: indexPath)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indice = sender as! Int
      let postDetailViewController = segue.destination as! PostDetailViewController
        postDetailViewController.post = list[indice]
        postDetailViewController.pvc = self
         
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if(UserDefault.integer(forKey: "signup") == 1)
        {
            
            bulletinManager.backgroundViewStyle = .blurredExtraLight
            bulletinManager.showBulletin(above: self)
            
            
        }
        
        
        
    
        
        floaty.sticky = true
        floaty.friendlyTap = true
        /*floaty.addItem("Add a post with bulletin",icon: UIImage(named:"plus")!,handler: {
            item in
            self.addBulletin()
        })*/
        floaty.addItem("Add a post", icon: UIImage(named: "plus")!, handler: { item in
            self.addAlert = UIAlertController(title: "Let's share", message: "please fill the fields", preferredStyle: .alert)
            self.addAlert.addTextField(configurationHandler: {(textfield : UITextField!) in
                textfield.placeholder = "Type your title here"
                self.titre = textfield
            })
            
            self.addAlert.addTextField(configurationHandler: {(textfield : UITextField!) in
                textfield.placeholder = "Type your description here"
                self.desc = textfield
            })
            self.addAlert.addAction(UIAlertAction(title:"Cancel",style: .cancel))
            self.addAlert.addAction(UIAlertAction(title:"Submit",style: .default){
                Void in
                
                self.confirm = UIAlertController(title: "Upload picture", message: "Do you want to upload a picture with the post ?", preferredStyle: .alert)
                self.confirm.addAction(UIAlertAction(title:"Choose picture",style: .default){
                    item in
                    let myPickerController = UIImagePickerController()
                        myPickerController.delegate = self;
                    myPickerController.sourceType =  UIImagePickerController.SourceType.photoLibrary
                    self.present(myPickerController, animated: true, completion: nil)
                    
                })
                self.confirm.addAction(UIAlertAction(title:"Just post it",style: .default){
                    i in
                    if(self.ValidateForm())
                    {
                        self.postController.AddPost(post: Post(title:self.titre.text!,description: self.desc.text!,picture:"empty" , user : self.UserDefault.integer(forKey: "user_id")), completionHandler: {
                            posts in
                         WelcomeNotif(title: "Post added \(self.UserDefault.string(forKey: "user_firstname")!) \(self.UserDefault.string(forKey: "user_lastname")!)", description: "Your recent post is added! enjoy Pet Rescue", picture: "Oasis")
                       self.list.append(posts)
                        
                        self.titi.reloadData()
                    }
                    )}
                    
                    
                })
                self.present(self.confirm,animated: true,completion: nil)
                self.floaty.close()            })
            self.present(self.addAlert, animated: true, completion: nil)
            
        })
        self.view.addSubview(floaty)
        
        postController.GetAllPost(completionHandler: {
            response in
            self.list = response 
            self.titi.reloadData()
        })
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        let picketImage = info[.originalImage] as! UIImage

        let imageData:Data = (picketImage ).jpegData(compressionQuality: 1.0)!
        let picURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL
        
        self.nom_image = picURL?.lastPathComponent
        
        UploadImage.upload(imageData: imageData,name:self.nom_image)
        if(self.ValidateForm())
        {self.postController.AddPost(post: Post(title:self.titre.text!,description: self.desc.text!,picture:self.nom_image , user : self.UserDefault.integer(forKey: "user_id")), completionHandler: {
        posts in
            
            WelcomeNotif(title: "Post added \(self.UserDefault.string(forKey: "user_firstname")!) \(self.UserDefault.string(forKey: "user_lastname")!)", description: "Your recent post is added! enjoy Pet Rescue", picture: "Oasis")
            self.list.append(posts)
            self.titi.reloadData()
        }
        )}
        
       // let imageStr = imageData.base64EncodedString()
//        UploadImage.upload(imageData: imageData)
       // SRWebClient.uploadImage(name: "", image: imageData)
        self.dismiss(animated: true, completion: nil)
    }
    
    func ValidateForm() -> Bool
    {
        if((self.titre.text?.trimmingCharacters(in:.whitespacesAndNewlines).isEmpty)!){
            self.titre.text = " You must insert title to add"
        }else if(  (self.desc.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!){
            self.desc.text = " You must type a description to add"
        }else
        {return true}
        return false
    }
    
    lazy var bulletinManager2: BLTNItemManager = {
        let introPage = BulletinDataSource.makeTextFieldPage(title: "Add Post", description: "Will you please fill the fields with the informations", button: "Add post")
        
        return BLTNItemManager(rootItem: introPage)
    }()
  
    func addBulletin()
    {
        if(UserDefault.integer(forKey: "signup") != 1)
       // let AddPage:TextFieldBulletinPage = TextFieldBulletinPage(title: "Add Post")
    
        //testPage.image = UIImage(named: "")
        
        {let AddPage:BLTNPageItem = BLTNPageItem(title: "Add Post")
        
        
        AddPage.image = UIImage(named: "")
        AddPage.descriptionText = "Will you please fill the fields with the informations"
        AddPage.actionButtonTitle = "Add post"
        AddPage.alternativeButtonTitle = "Cancel"
        AddPage.isDismissable = true
        let PicturePage:BLTNPageItem = BLTNPageItem(title: "Upload Picture")
        PicturePage.isDismissable = true
        PicturePage.actionButtonTitle = "Choose Picture"
        PicturePage.alternativeButtonTitle = "Return to post details"
        PicturePage.actionHandler = {
            (item:BLTNActionItem) in
            
            let myPickerController = UIImagePickerController()
                                  myPickerController.delegate = self;
                              myPickerController.sourceType =  UIImagePickerController.SourceType.photoLibrary
            PicturePage.manager?.dismissBulletin()
                              self.present(myPickerController, animated: true, completion: nil)
            
        }
        
        PicturePage.alternativeHandler = {
            (item:BLTNActionItem) in
            
            self.bulletinManager2.popItem()
        }
       // AddPage.manager?.push(item: PicturePage)
        AddPage.actionHandler = {
            (item:BLTNActionItem) in
            AddPage.manager?.displayActivityIndicator()
            AddPage.manager?.push(item: PicturePage)
            AddPage.manager?.displayNextItem()
            /*self.bulletinManager.push(item:PicturePage)
            self.bulletinManager.displayActivityIndicator()
        
            self.bulletinManager.displayNextItem()*/
        }
        
        AddPage.alternativeHandler = {
            (item:BLTNActionItem) in
            self.bulletinManager2.dismissBulletin(animated: true)
        }
        
        /*AddPage.manager?.backgroundViewStyle = .blurredExtraLight
        AddPage.manager?.showBulletin(above: self)*/
        
        bulletinManager2.backgroundViewStyle = .blurredExtraLight
        bulletinManager2.showBulletin(above: self)
        //bulletinManager2.push(item: testPage)}
        }else{
                bulletinManager.backgroundViewStyle = .blurredExtraLight
                bulletinManager.showBulletin(above: self)
            }
    }
    
    
    
    
}
   
/*class MyTapGesture : UITapGestureRecognizer {
    var post : Int = 0
    var body : String = ""
    var index : Int = 0
    var comment : Int = 0
}*/


