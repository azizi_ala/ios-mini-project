

import UIKit
import Alamofire
import AlamofireImage

class DetailsAdoptPetViewController: UIViewController {
    
    
   // @IBOutlet weak var userphoto: ImageRonde!
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var datepubText: UILabel!
    @IBOutlet weak var descText: UITextView!
    @IBOutlet weak var petimg: UIImageView!
    @IBOutlet weak var genderText: UILabel!
    @IBOutlet weak var ageText: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var adresse: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var usernamedetails: UILabel!
    @IBOutlet weak var nameofpet: UILabel!
    @IBOutlet weak var breedText: UILabel!
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var reqstatus: UILabel!
    
    @IBOutlet weak var userphoto: UIImageView!
    @IBOutlet weak var calllbl: UILabel!
    @IBOutlet weak var callbtn: UIButton!
    @IBOutlet weak var adoptionreqbtn: UIButton!
    var idpet:Int = 0
    var iduser:Int = 0
    var idowner:Int = 0
    var user:User = User()
    var petController = PetController()
    var userController = UserController()
    var notifController = NotifController()
    var adoptionController = AdoptionController()
    var pet = Pet()
    let UserDefault = UserDefaults.standard
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // let tapped = MyTapGesture.init(target: self, action: #selector(DetailsAdoptPetViewController.call))
        //phone.addGestureRecognizer(tapped)
        //phone.isUserInteractionEnabled = true
        refreshState()
        
        
         FetchData()
        
        
        
        
    }
   /* @objc func call(recognizer : MyTapGesture) {
         if let url = URL(string: "tel://\(self.user.user_phonenumber!)"),
               UIApplication.shared.canOpenURL(url) {
                  if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               } else {
    print("error")           }
    }*/
            
    
    @IBAction func BackBt(_ sender: Any) {
          dismiss(animated: true, completion: nil)}
    
    
   func FetchData() {

    // print(self.idpet)
     petController.GetPetById(id: idpet, completionHandler: {
            response in
        self.pet = response
        self.datepubText.text = "20/12/2019"
        self.breedText.text = response.pet_race
        self.genderText.text = response.pet_sex
        self.ageText.text = response.pet_age
        self.descText.text = response.pet_desc
        self.nameofpet.text = "Camping Name : \(response.pet_name!) "
        self.descText.isEditable = false
        let peturl = "http://127.0.0.1:3000/petrescue/public/img/\(response.pet_picture!)"
        self.petimg.af_setImage(withURL: URL(string:peturl)!)
        //self.descText.text = (jsonData.value(forKey: "descrip")as! String)
        //  let urlImage = Connexion.adresse + "images/" + ( jsonData.value(forKey: "petimg") as! String )
        //  let urlProfilImage = Connexion.adresse + "images/" + ( jsonData.value(forKey: "photo") as! String )
        //  self.userphoto.af_setImage(withURL:URL(string: urlProfilImage)!)
        //  self.petimg.af_setImage(withURL:URL(string:) urlImage)!
        
        
    })
    userController.GetUserById(id: "\(iduser)", completionHandler: {
              response in
        self.user = response
              self.usernameText.text = response.user_username
              self.owner.text = "\(response.user_firstname!) \(response.user_lastname!)"
              self.adresse.text = response.user_address
              self.phone.text = response.user_phonenumber
              self.email.text = response.user_email
        let urlImage = "http://127.0.0.1:3000/petrescue/public/img/\(response.user_picture!)"
        self.userphoto.af_setImage(withURL: URL(string: urlImage)!)
        if (response.user_id == self.UserDefault.integer(forKey: "user_id")){
            self.adoptionreqbtn.isHidden = true
            self.reqstatus.isHidden = true
            self.callbtn.isHidden = true
            self.calllbl.isHidden = true
        }
        
          })
    
    
    }
    @IBAction func btncall(_ sender: Any) {
         if let url = URL(string: "tel://\(self.user.user_phonenumber!)"),
               UIApplication.shared.canOpenURL(url) {
                  if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                   } else {
                       UIApplication.shared.openURL(url)
                   }
               } else {
    print("error")           }
            
        }
    
    @IBAction func requestclicked(_ sender: Any) {
        adoptionController.AddAdoption(adoption: Adoption(owner: self.user.user_id , adoptive: UserDefault.integer(forKey: "user_id") , pet: self.idpet,status: 1), completionHandler:{
            response in
            self.notifController.AddNotif(notif: Notif(user_id: self.pet.owner!, sender_id: self.UserDefault.integer(forKey: "user_id"), pet_id: self.pet.pet_id!, post_id: 0, type: "Adoption Request"), completionHandler: {response in

                self.reqstatus.text = "Request Pending"
                self.refreshState()
            })
            
        })
        
    }
    
    public func refreshState()
    {
        adoptionController.GetAdoptionByPet(Pet: idpet, completionHandler:{
            response in
            
            if (response.adoptive == self.UserDefault.integer(forKey: "user_id")){
                self.adoptionreqbtn.isHidden = true
                if (response.status == 1){
                    self.reqstatus.text = "Request Pending"
                    self.reqstatus.textColor = UIColor.gray
                }else if (response.status == 2){
                    self.reqstatus.text = "Request Rejected!"
                    self.reqstatus.textColor = UIColor.red
                }else if (response.status ==  3){
                    self.reqstatus.text = "Request Accepted!"
                    self.reqstatus.textColor = UIColor.green
                }
            }
            
            
        })
    }
    
}
    
    


