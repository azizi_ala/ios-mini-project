

import UIKit

class AccViewController: UIViewController {
    var user:User?
    @IBOutlet var Address: UILabel!
    @IBOutlet var Phone: UILabel!
    @IBOutlet var Fullname: UILabel!
    @IBOutlet var Username: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         let tabbar = tabBarController as! TabBarController
               user = tabbar.user;
        Address.text = user?.user_address
        Fullname.text = user?.user_username
        Username.text = user?.user_lastname
        Phone.text = user?.user_phonenumber
        
        
    }
    


}
