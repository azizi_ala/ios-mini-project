

import Foundation
import Alamofire
import SwiftyJSON

class CommentController {
    
    @discardableResult
    func getCommentsByPost(id:Int,completionhandler: @escaping ([Comment]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Comment]()
        
        return Alamofire.request("http://127.0.0.1:3000/petrescue/comments/getByPost/\(id)",method: .get).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Comment(json :i.1))
            }
            completionhandler(datas)
            
        }
    }
    
    @discardableResult
    func AddComment(post:Int,user:Int,body:String,completionhandler: @escaping ([Comment]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Comment]()
        let parameters : [String:Any] = [
            "user" : user,
            "post" : post,
            "body" : body
        ]
        return Alamofire.request("http://127.0.0.1:3000/petrescue/comments/add",method : .post ,parameters: parameters,encoding: JSONEncoding.default).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Comment(json :i.1))
            }
            completionhandler(datas)
        }
    }
    
    @discardableResult
    func DeleteComment(comment:Int,completionhandler: @escaping ([Comment]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Comment]()
        return Alamofire.request("http://127.0.0.1:3000/petrescue/comments/\(comment)",method : .delete).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Comment(json :i.1))
            }
            completionhandler(datas)
        }
    }
}
