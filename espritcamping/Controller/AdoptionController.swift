

import Foundation
import Alamofire
import SwiftyJSON

class AdoptionController {
   var adoption = Adoption()
    
    @discardableResult
       func GetAdoptionById(id:Int,completionHandler: @escaping (Adoption) -> Void ) -> Alamofire.DataRequest
       {
           var ad = Adoption()
          return Alamofire.request("http://127.0.0.1:3000/petrescue/mypets/"+String(id)).responseData    {(data) in
               let json = try! JSON(data: data.data!, options: .allowFragments)
               for i in json{
                   ad = Adoption(
                        adoption_id:i.1["adoption_id"].intValue ,
                        owner: i.1["owner"].intValue,
                        adoptive: i.1["adoptive"].intValue,
                        pet: i.1["pet"].intValue,
                        status: i.1["status"].intValue
                   )
                   completionHandler(ad)
               }
        }}
    
    @discardableResult
    func AddAdoption(adoption:Adoption,completionHandler: @escaping (Adoption)-> Void)-> Alamofire.DataRequest
    {
        let parameters:  [String : Any] = [
            "owner" : adoption.owner! ,
            "adoptive" : adoption.adoptive!,
            "pet" : adoption.pet! ,
            "status" : adoption.status!
            ]
            var datas = Adoption()
            return
    Alamofire.request("http://127.0.0.1:3000/petrescue/pet/adopt",method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{
                    adoption.adoption_id = i.1["adoption_id"].intValue
                    
                }
                completionHandler(datas)
        }
        }
    
    
    @discardableResult
          func GetAdoptionByPet(Pet:Int,completionHandler: @escaping (Adoption) -> Void ) -> Alamofire.DataRequest
          {
              var ad = Adoption()
             return Alamofire.request("http://127.0.0.1:3000/petrescue/adoption/pet/"+String(Pet)).responseData    {(data) in
                  let json = try! JSON(data: data.data!, options: .allowFragments)
                  for i in json{
                      ad = Adoption(
                           adoption_id:i.1["adoption_id"].intValue ,
                           owner: i.1["owner"].intValue,
                           adoptive: i.1["adoptive"].intValue,
                           pet: i.1["pet"].intValue,
                           status: i.1["status"].intValue
                      )
                      completionHandler(ad)
                  }
           }}
    
    
    @discardableResult
    func UpdateAdoption(adoption:Adoption,completionHandler: @escaping (Adoption)-> Void)-> Alamofire.DataRequest
    {
        let parameters:  [String : Any] = [
            "adoption_id" : adoption.adoption_id!,
            "owner" : adoption.owner! ,
            "adoptive" : adoption.adoptive!,
            "pet" : adoption.pet! ,
            "status" : adoption.status!
            ]
            var datas = Adoption()
            return
    Alamofire.request("http://127.0.0.1:3000/petrescue/pet/adopt/request",method: .put, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
               /* let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{
                    adoption.adoption_id = i.1["adoption_id"].intValue
                    
                }*/
                completionHandler(datas)
        }
        }
    
    @discardableResult
    func DeleteAdoption(post:Int,completionhandler: @escaping ([Adoption]) -> Void ) -> Alamofire.DataRequest
    {
        var i = post
        var datas = [Adoption]()
        return Alamofire.request("http://127.0.0.1:3000/petrescue/pet/adopt/delete/\(post)",method : .delete).responseJSON{
            response in
           /* let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Post(json :i.1))
            }*/
            completionhandler(datas)
        }
    }
    
    
    
}
