

import Foundation
import Alamofire
import SwiftyJSON

class LikeController {
    
    @discardableResult
    func getLikesByPost(id:Int,completionhandler: @escaping ([Likes]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Likes]()
        
        return Alamofire.request("http://127.0.0.1:3000/petrescue/like_posts/getAll/\(id)",method: .get).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Likes(json :i.1))
            }
            completionhandler(datas)
            
        }
    }
    
    @discardableResult
    func AddLike(post:Int,user:Int,completionhandler: @escaping ([Likes]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Likes]()
        let parameters : [String:Any] = [
            "user" : user,
            "post" : post
        ]
        return Alamofire.request("http://127.0.0.1:3000/petrescue/posts/like/\(user)/\(post)",method : .post ,parameters: parameters,encoding: JSONEncoding.default).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Likes(json :i.1))
            }
            completionhandler(datas)
        }
    }
    
    @discardableResult
    func Dislike(post:Int,user:Int,completionhandler: @escaping ([Likes]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Likes]()
        return Alamofire.request("http://127.0.0.1:3000/petrescue/posts/dislike/\(user)/\(post)",method : .delete).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Likes(json :i.1))
            }
            completionhandler(datas)
        }
    }
    
}
