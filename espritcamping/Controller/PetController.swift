


import Foundation
import Alamofire
import SwiftyJSON

class PetController {
    var pet = Pet()
    var pets = [Pet]()
    var UserControler = UserController()
    
    @discardableResult
    func GetPetById(id:Int,completionHandler: @escaping (Pet) -> Void ) -> Alamofire.DataRequest
    {
        var pt = Pet()
       return Alamofire.request("http://127.0.0.1:3000/petrescue/mypets/"+String(id)).responseData    {(data) in
            let json = try! JSON(data: data.data!, options: .allowFragments)
            for i in json{
                /*let user = self.UserControler.GetUser(id: i.1["owner"].stringValue)*/
                pt = Pet(
                    id : i.1["pet_id"].intValue,
                    name : i.1["pet_name"].stringValue,
                    race : i.1["pet_race"].stringValue,
                    age : i.1["pet_age"].stringValue,
                    picture : i.1["pet_picture"].stringValue,
                    sex : i.1["pet_sex"].stringValue,
                    desc : i.1["pet_desc"].stringValue,
                    status: i.1["pet_status"].stringValue,
                    owner : i.1["owner"].intValue
                )
                completionHandler(pt)
            }
        }
        
    }
    func GetLostedPet(status:String,completionHandler: @escaping (Pet) -> Void ) -> Alamofire.DataRequest
    {
        var pt = Pet()
       return Alamofire.request("http://127.0.0.1:3000/petrescue/mypets/"+String(status)).responseData    {(data) in
            let json = try! JSON(data: data.data!, options: .allowFragments)
            for i in json{
                /*let user = self.UserControler.GetUser(id: i.1["owner"].stringValue)*/
                pt = Pet(
                    id : i.1["pet_id"].intValue,
                    name : i.1["pet_name"].stringValue,
                    race : i.1["pet_race"].stringValue,
                    age: i.1["pet_age"].stringValue,
                    picture : i.1["pet_picture"].stringValue,
                    sex : i.1["pet_sex"].stringValue,
                    owner : i.1["owner"].intValue
                )
                completionHandler(pt)
            }
        }}
    
   
    
    @discardableResult
    func GetPetByRace(race:String?,completionHandler: @escaping ([Pet]) -> Void ) -> Alamofire.DataRequest
    {
        let parameters: [String: Any] = [
                "pet_race" : race as! String
            ]
            
            var datas = [Pet]()
        return
        Alamofire.request("http://127.0.0.1:3000/petrescue/pets/race",method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{ /*self.UserControler.GetUserById(id: i.1["owner"].stringValue)*/
                    datas.append(Pet(
                        id: i.1["pet_id"].intValue,
                        name: i.1["pet_name"].stringValue,
                        race: i.1["pet_race"].stringValue,
                        age: i.1["pet_age"].stringValue,
                        picture: i.1["pet_picture"].stringValue,
                        sex: i.1["pet_sex"].stringValue,
                        owner: i.1["owner"].intValue
                    )
                    )
                    
                }
               completionHandler(datas)
                
        }
        
    }
    
    @discardableResult
    func GetAllPet(completionHandler: @escaping ([Pet]) -> Void ) -> Alamofire.DataRequest
    {
            
            var datas = [Pet]()
        return
        Alamofire.request("http://127.0.0.1:3000/petrescue/pets",method: .get)
            .responseJSON{
                response in
                let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{ /*self.UserControler.GetUserById(id: i.1["owner"].stringValue)*/
                    datas.append(Pet(
                        id: i.1["pet_id"].intValue,
                        name: i.1["pet_name"].stringValue,
                        race: i.1["pet_race"].stringValue,
                        age: i.1["pet_age"].stringValue,
                        picture: i.1["pet_picture"].stringValue,
                        sex: i.1["pet_sex"].stringValue,
                        owner: i.1["owner"].intValue
                    )
                    )
                    
                }
               completionHandler(datas)
                
        }
        
    }
    @discardableResult
    func AddPet(animal:Pet,completionHandler: @escaping (Pet)-> Void)-> Alamofire.DataRequest
    {
        let parameters:  [String : Any] = [
            "pet_name" : animal.pet_name!,
            "pet_race" : animal.pet_race!,
            "pet_age" : animal.pet_age!,
            "pet_picture" : animal.pet_picture! ,
            "pet_status" : animal.pet_status! ,
            "pet_sex" : animal.pet_sex! ,
            "pet_desc" : animal.pet_desc! ,
            "owner" : animal.owner!
            ]
        let datas = Pet() 
            return
    Alamofire.request("http://127.0.0.1:3000/petrescue/mypets",method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{
                    animal.pet_id = i.1["pet_id"].intValue
                    
                }
                completionHandler(datas)
        }
        }
        
    @discardableResult
       func getPetsByOwner(id:Int,completionhandler: @escaping ([Pet]) -> Void ) -> Alamofire.DataRequest
       {
           var datas = [Pet]()
           
           return Alamofire.request("http://127.0.0.1:3000/petrescue/pets/owner/\(id)",method: .get).responseJSON{
               response in
               let json = try! JSON(data:response.data!,options: .allowFragments)
               for i in json{
                   datas.append(Pet(json :i.1))
               }
               completionhandler(datas)
               
           }
       }
    
    
      @discardableResult
      func DeletePet(id:Int,completionhandler: @escaping ([Pet]) -> Void ) -> Alamofire.DataRequest
      {
          var datas = [Pet]()
          return Alamofire.request("http://127.0.0.1:3000/petrescue/mypets/\(id)",method : .delete).responseJSON{
              response in
              let json = try! JSON(data:response.data!,options: .allowFragments)
              for i in json{
                  datas.append(Pet(json :i.1))
              }
              completionhandler(datas)
          }
      }
    
    
    @discardableResult
       func UpdatePet(animal:Pet,completionHandler: @escaping (Pet)-> Void)-> Alamofire.DataRequest
       {
        let parameters:  [String : Any] = [
        "pet_id" : animal.pet_id!,
        "pet_name" : animal.pet_name! ,
        "pet_race" : animal.pet_race!,
        "pet_age" : animal.pet_age! ,
        "pet_picture" : animal.pet_picture! ,
        "pet_status" : animal.pet_status! ,
        "pet_sex" : animal.pet_sex! ,
        "pet_desc" : animal.pet_desc! ,
        "owner" : animal.owner!
        ]
            var datas = Pet()
                   return Alamofire.request("http://127.0.0.1:3000/petrescue/mypets/",method : .put,parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                       response in
                       let json = try! JSON(data:response.data!,options: .allowFragments)
                       for i in json{
                           datas = Pet(json :i)
                       }
                       completionHandler(datas)
                   }
           }
    
    @discardableResult
    func UpdateOwner(owner:Int,pet:Int,completionHandler: @escaping (Pet)-> Void)-> Alamofire.DataRequest
    {
         var datas = Pet()
                return Alamofire.request("http://127.0.0.1:3000/petrescue/pets/owner/\(owner)/\(pet)",method : .put).responseJSON{
                    response in
                    /*let json = try! JSON(data:response.data!,options: .allowFragments)
                    for i in json{
                        datas = Pet(json :i)
                    }*/
                    completionHandler(datas)
                }
        }

}

