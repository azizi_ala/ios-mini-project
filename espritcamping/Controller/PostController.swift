

import UIKit
import Alamofire
import SwiftyJSON

class PostController: UIViewController {
    var post = Post()
    var posts = [Post]()
    var UserControler = UserController()
    
    
    @discardableResult
    func GetPostById(id:Int,completionHandler: @escaping (Post) -> Void ) -> Alamofire.DataRequest
    {
        print(id)
        var pt = Post()
       return Alamofire.request("http://127.0.0.1:3000/petrescue/posts/"+String(id)).responseData    {(data) in
            let json = try! JSON(data: data.data!, options: .allowFragments)
            for i in json{
                /*let user = self.UserControler.GetUser(id: i.1["owner"].stringValue)*/
                pt = Post(
                id: i.1["post_id"].intValue,
                title: i.1["post_title"].stringValue,
                description: i.1["post_description"].stringValue,
                date: i.1["post_date"].stringValue,
                picture: i.1["post_image"].stringValue,
                user: i.1["post_user"].intValue,
                local: i.1["post_localisation"].stringValue
                )
                completionHandler(pt)
            }
        }
        
    }
    @discardableResult
    func GetPostByOwner(id : Int,completionHandler: @escaping ([Post]) -> Void ) -> Alamofire.DataRequest
    {
        print("http://127.0.0.1:3000/petrescue/myposts/\(id)")
        var datas = [Post]()
    return Alamofire.request("http://127.0.0.1:3000/petrescue/myposts/\(id)",method: .get)
        .responseJSON{
            response in
            let json = try! JSON(data: response.data!, options: .allowFragments)
            for i in json{ /*self.UserControler.GetUserById(id: i.1["owner"].stringValue)*/
                datas.append(Post(
                    id: i.1["post_id"].intValue,
                    title: i.1["post_title"].stringValue,
                    description: i.1["post_description"].stringValue,
                    date: i.1["post_date"].stringValue,
                    picture: i.1["post_image"].stringValue,
                    user: i.1["post_user"].intValue,
                    local: i.1["post_localisation"].stringValue
                ))
            }
            completionHandler(datas)
        }
    }
    
    @discardableResult
    func GetAllPost(completionHandler: @escaping ([Post]) -> Void ) -> Alamofire.DataRequest
    {
            
            var datas = [Post]()
        return
        Alamofire.request("http://127.0.0.1:3000/petrescue/posts",method: .get)
            .responseJSON{
                response in
                let json = try! JSON(data: response.data!, options: .allowFragments)
                for i in json{ /*self.UserControler.GetUserById(id: i.1["owner"].stringValue)*/
                    datas.append(Post(
                        id: i.1["post_id"].intValue,
                        title: i.1["post_title"].stringValue,
                        description: i.1["post_description"].stringValue,
                        date: i.1["post_date"].stringValue,
                        picture: i.1["post_image"].stringValue,
                        user: i.1["post_user"].intValue,
                        local: i.1["post_localisation"].stringValue
                    )
                    )
                    
                }
               completionHandler(datas)
                
        }
        
    }
    
   @discardableResult
    func AddPost(post : Post,completionHandler: @escaping (Post) -> Void ) -> Alamofire.DataRequest
    {
        let parameters: [String: Any] = [
            "post_title" : post.post_title!,
            "user":[ "post_user" : post.post_user],
            "post_description" : post.post_description!,
            "post_image" : post.post_picture!
        ]
         var datas = Post()
        return Alamofire.request("http://127.0.0.1:3000/petrescue/posts",method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let i = try! JSON(data: response.data!, options: .allowFragments)
                     datas = Post(
                         id: i["post_id"].intValue,
                         title: i["post_title"].stringValue,
                         description: i["post_description"].stringValue,
                         date: i["post_date"].stringValue,
                         picture: i["post_image"].stringValue,
                         user: i["post_user"].intValue,
                         local: i["post_localisation"].stringValue
                     )
                     
                completionHandler(datas)
                
               
        }
    
    }
    
    @discardableResult
    func DeletePost(post:Int,completionhandler: @escaping ([Post]) -> Void ) -> Alamofire.DataRequest
    {
        var datas = [Post]()
        return Alamofire.request("http://127.0.0.1:3000/petrescue/myposts/\(post)",method : .delete).responseJSON{
            response in
            let json = try! JSON(data:response.data!,options: .allowFragments)
            for i in json{
                datas.append(Post(json :i.1))
            }
            completionhandler(datas)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}
