

import Foundation
import Alamofire
import SwiftyJSON
class UserController
{
    var user = User();
    
    @discardableResult
    func GetUserByEmail(id: String,completionHandler: @escaping (User) -> Void ) -> Alamofire.DataRequest
    {
        return Alamofire.request("http://127.0.0.1:3000/petrescue/user/email/"+id)
            .responseJSON {
                
                (data) in
                let json = try! JSON(data: data.data!, options: .allowFragments)
                
                for i in json{
                    let userReponse = User(
                        id: i.1["user_id"].intValue,
                        email: i.1["user_email"].stringValue,
                        username:i.1["user_username"].stringValue,
                        pwd:i.1["password"].stringValue,
                        firstname:i.1["user_firstname"].stringValue,
                        lastname:i.1["user_lastname"].stringValue,
                        picture:i.1["user_picture"].stringValue,
                        address:i.1["user_address"].stringValue,
                        phonenumber:i.1["user_phonenumber"].stringValue
                    )
                    completionHandler(userReponse)
        }
                
        }
    }
    
    
    @discardableResult
    func GetUserById(id :String,completionHandler: @escaping (User) -> Void ) -> Alamofire.DataRequest
    {
        print(id)
        return Alamofire.request("http://127.0.0.1:3000/petrescue/user/"+id)
            .responseJSON {
                
                (data) in
                let json = try! JSON(data: data.data!, options: .allowFragments)
                
                for i in json{
                    let userReponse = User(
                        id: i.1["user_id"].intValue,
                        email: i.1["user_email"].stringValue,
                        username:i.1["user_username"].stringValue,
                        pwd:i.1["password"].stringValue,
                        firstname:i.1["user_firstname"].stringValue,
                        lastname:i.1["user_lastname"].stringValue,
                        picture:i.1["user_picture"].stringValue,
                        address:i.1["user_address"].stringValue,
                        phonenumber:i.1["user_phonenumber"].stringValue
                    )
                    completionHandler(userReponse)
        }
        }
    }
    
    func getAllUsers() -> [User]
    {
        var datas = [User]()
        Alamofire.request("http://127.0.0.1:3000/petrescue/user").responseData    {(data) in
            let json = try! JSON(data: data.data!, options: .allowFragments)
            for i in json{
                datas.append(User(
                    id: i.1["user_id"].intValue,
                    email: i.1["user_email"].stringValue,
                    username:i.1["user_username"].stringValue,
                    pwd:i.1["password"].stringValue,
                    firstname:i.1["user_firstname"].stringValue,
                    lastname:i.1["user_lastname"].stringValue,
                    picture:i.1["user_picture"].stringValue,
                    address:i.1["user_address"].stringValue,
                    phonenumber:i.1["user_phonenumber"].stringValue
                    
                ))
            }
            
        }
        return datas;
    }
    
    @discardableResult
    func Login(email:String , password:String, completionHandler: @escaping (User) -> Void ) -> Alamofire.DataRequest
    {return Alamofire.request("http://127.0.0.1:3000/petrescue/user/"+email+"/"+password)
            .responseJSON {
                
                (data) in
                let json = try! JSON(data: data.data!, options: .allowFragments)
                
                for i in json{
                    let Reponse = User(
                        id: i.1["user_id"].intValue,
                        email: i.1["user_email"].stringValue,
                        username:i.1["user_username"].stringValue,
                        pwd:i.1["password"].stringValue,
                        firstname:i.1["user_firstname"].stringValue,
                        lastname:i.1["user_lastname"].stringValue,
                        picture:i.1["user_picture"].stringValue,
                        address:i.1["user_address"].stringValue,
                        phonenumber:i.1["user_phonenumber"].stringValue
                    )
                    completionHandler(Reponse)
        }
        }
    }
    @discardableResult
    func SignUp(utilisateur : User,completionHandler: @escaping (User) -> Void ) -> Alamofire.DataRequest
    {
        let parameters: [String: Any] = [
            "user_email" : utilisateur.user_email,
            "password" : utilisateur.password,
            "user_username" : utilisateur.user_username,
            "user_phonenumber" : utilisateur.user_phonenumber,
            "user_firstname" : "Firstname",
            "user_lastname" : "Lastname",
            "user_picture" : "",
            "user_address" : ""
        ]
        return Alamofire.request("http://127.0.0.1:3000/petrescue/signup",method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let i = try! JSON(data: response.data!, options: .allowFragments)
                    let up = User(
                        id: i["user_id"].intValue,
                        email: i["user_email"].stringValue,
                        username:i["user_username"].stringValue,
                        pwd:i["password"].stringValue,
                        firstname:i["user_firstname"].stringValue,
                        lastname:i["user_lastname"].stringValue,
                        picture:i["user_picture"].stringValue,
                        address:i["user_address"].stringValue,
                        phonenumber:i["user_phonenumber"].stringValue
                    )
                     completionHandler(up)
                
               
        }
    
    }
    
    @discardableResult
    func Update(utilisateur : User,completionHandler: @escaping (User) -> Void ) -> Alamofire.DataRequest
    {
        let parameters: [String: Any] = [
            "user_id" : utilisateur.user_id!,
            "user_email" : utilisateur.user_email!,
            "user_username" : utilisateur.user_username!,
            "user_phonenumber" : utilisateur.user_phonenumber,
            "user_firstname" : utilisateur.user_firstname!,
            "user_lastname" : utilisateur.user_lastname!,
            "user_picture" : utilisateur.user_picture!,
            "user_address" : utilisateur.user_address!
        ]
        return Alamofire.request("http://127.0.0.1:3000/petrescue/myaccount",method: .put, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON{
                response in
                let i = try! JSON(data: response.data!, options: .allowFragments)
                    let up = User(
                        id: i["user_id"].intValue,
                        email: i["user_email"].stringValue,
                        username:i["user_username"].stringValue,
                        pwd:i["password"].stringValue,
                        firstname:i["user_firstname"].stringValue,
                        lastname:i["user_lastname"].stringValue,
                        picture:i["user_picture"].stringValue,
                        address:i["user_address"].stringValue,
                        phonenumber:i["user_phonenumber"].stringValue
                    )
                     completionHandler(up)
                
               
        }
    
    }
    
    
    
    
    
}
