
import Foundation
import Alamofire
import SwiftyJSON

class NotifController {
    var notif = Notif()
    
    @discardableResult
    func GetNotifByUser(user:Int,completionHandler: @escaping ([Notif]) -> Void ) -> Alamofire.DataRequest
    {
        var notif = [Notif]()
       return Alamofire.request("http://127.0.0.1:3000/petrescue/notification/"+String(user)).responseJSON   {(data) in
       
        let json = try! JSON(data: data.data!, options: .allowFragments)
            for i in json{
                notif.append( Notif(
                     notification_id:i.1["notification_id"].intValue,
                     user_id:i.1["user_id"].intValue,
                     sender_id: i.1["sender_id"].intValue,
                     pet_id: i.1["pet_id"].intValue,
                     post_id: i.1["post_id"].intValue,
                     type: i.1["type"].stringValue,
                     date: i.1["date"].stringValue
                ))
                completionHandler(notif)
            }
     }}
    @discardableResult
       func AddNotif(notif:Notif,completionHandler: @escaping (Notif)-> Void)-> Alamofire.DataRequest
       {
           let parameters:  [String : Any] = [
               "user_id" : notif.user_id!,
               "sender_id" : notif.sender_id!,
               "pet_id" : notif.pet_id!,
               "post_id" : notif.post_id!,
               "type" : notif.type!
               ]
               let datas = Notif()
               return
       Alamofire.request("http://127.0.0.1:3000/petrescue/notification",method: .post, parameters: parameters, encoding: JSONEncoding.default)
               .responseJSON{
                   response in
                   /*let json = try! JSON(data: response.data!, options: .allowFragments)
                   for i in json{
                       notif.notification_id = i.1["notification_id"].intValue
                       
                   }*/
                   completionHandler(datas)
           }
           }
    
    
    @discardableResult
        func DeleteNotif(id:Int,completionhandler: @escaping ([Notif]) -> Void ) -> Alamofire.DataRequest
        {
            var datas = [Notif]()
            return Alamofire.request("http://127.0.0.1:3000/petrescue/notification/\(id)",method : .delete).responseJSON{
                response in
        /*        let json = try! JSON(data:response.data!,options: .allowFragments)
                for i in json{
                    datas.append(Notif(json :i.1))
                }*/
                completionhandler(datas)
            }
        }
      
    
    
    
}
