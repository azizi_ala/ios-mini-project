import UIKit
import UIKit
import Alamofire
import AlamofireImage
import Photos
import SDWebImage

class UpdateProfilViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var firstname: UITextField!
    //Utils
    var pickedImageProduct = UIImage()
    let imagePicker = UIImagePickerController()
    var imageName:String = ""
    let UserDefault = UserDefaults.standard
    let userControlller:UserController = UserController()
    //Outlets
   
    @IBOutlet weak var imageProfil: SDAnimatedImageView?
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var telephone: UITextField!
    
    @IBOutlet weak var adresse: UITextField!
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var countrylabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.username.text = UserDefault.string(forKey: "user_username")!
        self.email.text = UserDefault.string(forKey: "user_email")!
        self.telephone.text = UserDefault.string(forKey: "user_phonenumber")!
         self.adresse.text = UserDefault.string(forKey: "user_address")!
        self.firstname.text = UserDefault.string(forKey: "user_firstname")!
        self.lastname.text = UserDefault.string(forKey: "user_lastname")!
        self.imageName = UserDefault.string(forKey: "user_picture")!
        self.imageProfil?.layer.borderWidth = 1
        self.imageProfil?.layer.masksToBounds = false
        self.imageProfil?.layer.borderColor = UIColor.black.cgColor
        self.imageProfil?.layer.cornerRadius = (self.imageProfil?.frame.height)!/2
        self.imageProfil?.clipsToBounds = true
        self.imageProfil?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(UserDefault.string(forKey: "user_picture")!)"),placeholderImage: UIImage(named:"profile_round"))
        
        let tapped = MyTapGesture.init(target: self, action: #selector(UpdateProfilViewController.location))
        countrylabel.addGestureRecognizer(tapped)
        countrylabel.isUserInteractionEnabled = true
        adresse.addGestureRecognizer(tapped)
        adresse.isUserInteractionEnabled = true
    }
    
    
    @IBAction func ChangeImage(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Error: \(info)")
        }
        
        let imageData:Data = (selectedImage ).jpegData(compressionQuality: 1.0)!
        let picURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL
        self.imageName = picURL!.lastPathComponent
        UploadImage.upload(imageData: imageData, name: self.imageName)
        self.imageProfil?.sd_setImage(with:URL(string:"http://127.0.0.1:3000/petrescue/public/img/\(UserDefault.string(forKey: "user_picture")!)"),placeholderImage: UIImage(named:"round_picture"))
        self.dismiss(animated: true, completion: nil)
        //self.imageProfil?.image = selectedImage
        /*pickedImageProduct = selectedImage
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as! URL
        print(fileUrl.lastPathComponent)
        self.imageProfil.image = pickedImageProduct
        self.dismiss(animated: true, completion: nil)*/
    }
    @IBAction func update(_ sender: UIButton) {
        userControlller.Update(utilisateur: User(id: UserDefault.integer(forKey: "user_id"), email: email.text!, username: username.text!, pwd: UserDefault.string(forKey: "password"), firstname: firstname.text!, lastname: lastname.text!, picture: imageName, address: adresse.text!, phonenumber: telephone.text!), completionHandler: {
            response in
            self.UserDefault.removeObject(forKey: "signup")
            self.UserDefault.set(0,forKey:"signup")
        })
        
    }
    @objc func location(recognizer : MyTapGesture){
    let alert = UIAlertController(style: .actionSheet)
        alert.addLocationPicker { location in
            let sub = location?.placemark.locality
            let country = location?.placemark.country
            
            self.adresse.text = "\(sub!), \(country!)"
    }
    alert.addAction(title: "Cancel", style: .cancel)
    self.present(alert, animated: true, completion: nil)
    }
    
}
